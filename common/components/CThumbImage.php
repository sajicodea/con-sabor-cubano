<?php
namespace common\components;

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

/**
 * Description of ThumbImage
 *
 * @author saji
 */
use Yii;
use yii\imagine\Image;
use yii\helpers\Url;


class CThumbImage {

    const UPLOAD_PATH = '@docroot/uploads';


    //put your code here
    public static function create($image_name, $width,$height){

        $path       = Yii::getAlias(self::UPLOAD_PATH.DIRECTORY_SEPARATOR);
        $thumb_path = Yii::getAlias(self::UPLOAD_PATH.DIRECTORY_SEPARATOR."thumbnail");

        $given_image = explode("/", $image_name);
        $filename    = end($given_image);
        array_pop($given_image);

        //creating the folder
        if(!file_exists($thumb_path)){
            mkdir($thumb_path);
        }

        $path .= implode(DIRECTORY_SEPARATOR,$given_image);

        $given_image[] = $width."x".$height;
        if($given_image){
            foreach($given_image as $pa){
                $thumb_path = $thumb_path.DIRECTORY_SEPARATOR.$pa;
                if(!file_exists($thumb_path)){
                    mkdir($thumb_path);
                }
            }
        }


        if(!file_exists($path.DIRECTORY_SEPARATOR.$filename)){
            return false;
        }

        $given_image[]=$filename;

        if(!file_exists($thumb_path.DIRECTORY_SEPARATOR.$filename)){
            if(Image::thumbnail($path . DIRECTORY_SEPARATOR. $filename, $width, $height)->save($thumb_path.DIRECTORY_SEPARATOR. $filename, ['quality' => 80])){
                Yii::$app->db->createCommand()->insert('thumbimages', [
                    'code' => count($given_image)>2 ? $given_image[0] : '',
                    'name' => $filename,
                    'path' => $thumb_path.DIRECTORY_SEPARATOR. $filename,
                ])->execute();

                if(Yii::$app->has('urlManagerFrontEnd')){
                    return Yii::$app->urlManagerFrontEnd->baseUrl."/uploads/thumbnail/".implode("/", $given_image);
                }
                return Url::to('@web/uploads/thumbnail/').implode("/", $given_image);
            }
            return false;
        }
        if(Yii::$app->has('urlManagerFrontEnd')) {
            return Yii::$app->urlManagerFrontEnd->baseUrl."/uploads/thumbnail/".implode("/", $given_image);
        }
        return Url::to('@web/uploads/thumbnail/').implode("/", $given_image);
    }

    public static function deleteThumbeImages($image_name,$code=false)
    {
        $result_query = (new \yii\db\Query)->select('id,name,path')->from('thumbimages')->where(['name' => $image_name]);
        if($code){
            $result_query->andFilterWhere(['code' => $code]);
        }
        $results = $result_query->all();
        if($results){
            $ids = array();
            foreach($results as $result){
                if(file_exists($result['path'])){
                    unlink($result['path']);
                }
                $ids[]=$result['id'];
            }
            Yii::$app->db->createCommand("DELETE FROM thumbimages WHERE id IN ('".implode("', '",$ids)."')")->execute();
        }
    }
}
