<?php

namespace common\models;

use common\components\CThumbImage;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "instructor".
 *
 * @property integer $id
 * @property integer $staff_id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 */
class Instructor extends \yii\db\ActiveRecord
{
    const IMG_UPLOAD_DIR = '@docroot/uploads/instructor/';
    public $old_image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'instructor';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'image', 'status'], 'required' , 'on' => 'create'],
            [['name', 'description', 'status'], 'required' , 'on' => 'update'],
            [['staff_id', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['image'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg','maxSize' => 1024 * 1024 * 5,'minHeight' =>100,'minWidth'=>100, 'maxFiles' => 10, 'checkExtensionByMimeType'=>false],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'name' => 'Name',
            'description' => 'Description',
            'image' => 'Image',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /***
     * Creating image
     */
    public function upload()
    {
        $path = Yii::getAlias(self::IMG_UPLOAD_DIR);
        if(!$this->image) {
            return true;
        }

        $image = Yii::$app->security->generateRandomString(). '.' . $this->image->extension;
        if($this->image->saveAs($path .DIRECTORY_SEPARATOR. $image)){
            $this->image = $image;
        }

        return true;
    }

    /***
     * Deleting featured image
     */
    public function deleteImage()
    {
        $path = Yii::getAlias(self::IMG_UPLOAD_DIR);
        if(file_exists($path .DIRECTORY_SEPARATOR.$this->old_image)){
            unlink($path .DIRECTORY_SEPARATOR.$this->old_image);
        }

        CThumbImage::deleteThumbeImages($this->old_image);
    }

    /***
     * Featured Thumb URL
     */
    public function getImageThumbUrl($width,$height)
    {
        return CThumbImage::create("instructor/".$this->image,$width, $height);
    }
}
