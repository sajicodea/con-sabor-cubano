<?php

namespace common\Models;

use Yii;

/**
 * This is the model class for table "testimonial".
 *
 * @property integer $id
 * @property string $email
 * @property string $description
 * @property string $name
 * @property integer $status
 */
class Testimonial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testimonial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'description', 'name'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'description' => 'Testimonial',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    public function sendEmail($email)
    {
        $url = Yii::$app->urlManagerFrontEnd->baseUrl.'/admin/testimonial';
        $body  = 'Hello,<br /><br />';
        $body .= '<table border="1" cellpadding="10">';
        $body .= '<tr><td>Name</td><td>:</td><td>'.$this->name.'</td></tr>';
        $body .= '<tr><td>Email</td><td>:</td><td>'.$this->email.'</td></tr>';
        $body .= '<tr><td>Description</td><td>:</td><td>'.$this->description.'</td></tr>';
        $body .= '</table>';
        $body .= "<br /><br />To approve the testimonial, Please click <a href='$url'>here</a>";

        $body .= "<br /><br />Thank you,<br />".Yii::$app->name;

        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([\Yii::$app->params['supportEmail'] => 'CON SABOR CUBANO'])
            ->setSubject('Testimonial Approval  : '.Yii::$app->name)
            //->setTextBody($body_text)
            ->setHtmlBody($body)
            ->send();
        return true;
    }
}
