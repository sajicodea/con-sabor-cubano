<?php

namespace common\Models;

use common\components\CThumbImage;
use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $status
 */
class Gallery extends \yii\db\ActiveRecord
{
    const IMG_UPLOAD_DIR = '@docroot/uploads/gallery/';
    public $old_image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'status'], 'required','on' => 'create'],
            [['status'], 'required','on' => 'update'],
            [['status'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
            [['image'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg','maxSize' => 1024 * 1024 * 5,'minHeight' =>100,'minWidth'=>100, 'maxFiles' => 10, 'checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'status' => 'Status',
        ];
    }

    /***
     * Creating image
     */
    public function upload()
    {
        $path = Yii::getAlias(self::IMG_UPLOAD_DIR);
        if(count($this->image)<= 0) {
            return true;
        }
        foreach ($this->image as $file) {
            $image = Yii::$app->security->generateRandomString(). '.' . $file->extension;
            if($file->saveAs($path .DIRECTORY_SEPARATOR. $image)){
                $gallery = new Gallery();
                $gallery->title = $this->title;
                $gallery->image = $image;
                $gallery->status = $this->status;
                $gallery->save(false);
            }
        }

        return true;
    }

    /***
     * Deleting featured image
     */
    public function deleteImage()
    {
        $path = Yii::getAlias(self::IMG_UPLOAD_DIR);
        if(file_exists($path .DIRECTORY_SEPARATOR.$this->old_image)){
            unlink($path .DIRECTORY_SEPARATOR.$this->old_image);
        }

        CThumbImage::deleteThumbeImages($this->old_image);
    }

    /***
     * Featured Thumb URL
     */
    public function getImageThumbUrl($width,$height)
    {
        return CThumbImage::create("gallery/".$this->image,$width, $height);
    }
}
