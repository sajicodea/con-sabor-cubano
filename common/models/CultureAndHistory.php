<?php

namespace common\Models;

use Yii;

/**
 * This is the model class for table "culture_and_history".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $status
 * @property integer $created_at
 */
class CultureAndHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'culture_and_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'status'], 'required'],
            [['description', 'status'], 'string'],
            [['created_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(!Yii::$app->user->isGuest) {
            if (parent::beforeSave($insert)) {
                if($insert) {
                    $this->created_at = time();
                }

                return true;
            } else {
                return false;
            }
        }

        return true;
    }
}
