<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@docroot/uploads/',
            'uploadUrl' =>'http://con-sabor-cubano.codeaweb.net/uploads/',
            'imageAllowExtensions'=>['jpg','png','gif']
        ],
        'blog' => [
            'class' => 'funson86\blog\Module',
            'controllerNamespace' => 'funson86\blog\controllers\backend'
        ],
    ],
];
