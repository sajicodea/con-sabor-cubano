<?php

/* @var $this yii\web\View */
use yii\bootstrap\Html;

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="page-header">
        <h1>Welcome to <?= Yii::$app->name;?> Admin Panel</h1>
    </div>

    <p class="lead">Start site administration with the following:</p>

    <div class="row">
        <?php foreach ($links as $link): ?>
            <div class="generator col-lg-4">
                <h3><?= Html::encode($link['label']) ?></h3>
                <p><?= Html::encode($link['description']) ?></p>
                <p><?= Html::a('Start »', $link['url'], ['class' => 'btn btn-default']) ?></p>
            </div>
        <?php endforeach;  ?>
    </div>
</div>
