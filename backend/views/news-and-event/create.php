<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsAndEvent */

$this->title = 'Create News And Event';
$this->params['breadcrumbs'][] = ['label' => 'News And Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-and-event-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
