<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CultureAndHistory */

$this->title = 'Create Culture And History';
$this->params['breadcrumbs'][] = ['label' => 'Culture And Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="culture-and-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
