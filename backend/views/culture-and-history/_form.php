<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CultureAndHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="culture-and-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(\yii\redactor\widgets\Redactor::className(),
        [
            'clientOptions' => [
                'minHeight'=>300,
                'maxHeight'=>800,
                'plugins' => ['clips', 'fontcolor','imagemanager'],
                'replaceDivs' => false
            ],
        ])
    ?>

    <?= $form->field($model, 'status')->dropDownList([ 1 => 'Enabled', 0 => 'Disabled', ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
