<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CultureAndHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Culture And Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="culture-and-history-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Culture And History', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'value' => function($data) {
                    return $data->status ==1 ? "Enabled" : "Disabled";
                },
                'attribute' => 'status',
                'filter' => [0=>'Disabled',1 => 'Enabled'],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
