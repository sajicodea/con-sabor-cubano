<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Block */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="block-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($model->isNewRecord){ ?>
    <?= $form->field($model, 'block_key')->textInput(['maxlength' => true]) ?>
    <?php }else{ ?>
    <?= $form->field($model, 'block_key')->textInput(['maxlength' => true,'readonly'=>'readonly']) ?>
    <?php } ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(\yii\redactor\widgets\Redactor::className(), [
                'clientOptions' => [
                //'imageManagerJson' => ['/redactor/upload/image-json'],
                //'imageUpload' => ['/redactor/upload/image'],
                //'fileUpload' => ['/redactor/upload/file'],
                //'lang' => 'zh_cn',
                'minHeight'=>300,
                'maxHeight'=>800,
                'plugins' => ['clips', 'fontcolor','imagemanager'],
                'replaceDivs' => false,   
                'linebreaks'=> true,
                'enterKey' => false    
            ]
            ]) ?>

    <?= $form->field($model, 'status')->dropDownList([1=>'Enabled',0=>'Disabled']) ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
