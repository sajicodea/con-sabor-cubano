<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name.' | Admin Panel',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] =  ['label' => 'Testimonials', 'url' => ['/testimonial/index']];
        $menuItems[] =  ['label' => 'Gallery', 'url' => ['/gallery/index']];
        $menuItems[] =
            ['label' => 'Contents', 'url' => ['/blog/index'],'options'=> ['class' => 'dropdown'], 'items' => [
                ['label' => 'Pages', 'url' => ['/page/index']],
                ['label' => 'Block', 'url' => ['/block/index']],
            ]];
        $menuItems[] =  ['label' => 'Instructors', 'url' => ['/instructor/index']];
        $menuItems[] = ['label' => 'Culture and History', 'url' => ['/culture-and-history/index'],'description' => 'Manage Culture and History'];
        $menuItems[] =  ['label' => 'News And Events', 'url' => ['//news-and-event/index']];
        $menuItems[] =
            ['label' => 'Blog', 'url' => ['/blog/index'],'options'=> ['class' => 'dropdown'], 'items' => [
                ['label' => 'Blog Post', 'url' => ['/blog/blog-post']],
                ['label' => 'Blog Category', 'url' => ['/blog/blog-catalog']],
                ['label' => 'Blog Comment', 'url' => ['/blog/blog-comment']],
                ['label' => 'Blog Tag', 'url' => ['/blog/blog-tag']],
            ]];
        $menuItems[] =
            ['label' => 'My Account', 'items' => [
                ['label' => 'Update profile', 'url' =>['/site/profile','id' => \yii::$app->user->id]],
                ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']]
            ]];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy;  <?= Yii::$app->name.' '.date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
