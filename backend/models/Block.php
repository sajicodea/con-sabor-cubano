<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "block".
 *
 * @property integer $id
 * @property string $block_key
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 */
class Block extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%block}}';
    }

    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),            
        ];
        
    }    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block_key', 'title', 'content', 'status'], 'required'],
            [['content'], 'string','max'=>10000],
            [['status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['block_key', 'title'], 'string', 'max' => 128],
            [['block_key'],function($attribute, $params){
                if(preg_match('/[^a-z_]/', $this->$attribute) || (preg_match('/[_]/', $this->$attribute) && preg_match('/[^a-z_]/', $this->$attribute))){
                     $this->addError($attribute, 'Key must contain only lowercase letters or underscore.');
                }
            }],
            ['block_key','unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'block_key' => 'Block Key',
            'title' => 'Title',
            'content' => 'Content',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
