<?php
namespace backend\controllers;

use backend\models\Admin;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use backend\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $links = [
            ['label' => 'Pages', 'url' => ['/page/index'],'description' => 'Manage pages'],
            ['label' => 'Blogs', 'url' => ['/blog'],'description' => 'Manage Subscribers'],
            ['label' => 'Testimonials', 'url' => ['/testimonial/index'],'description' => 'Manage testimonials'],
            ['label' => 'Culture and History', 'url' => ['/culture-and-history/index'],'description' => 'Manage Culture and History'],
            ['label' => 'Galleries', 'url' => ['/gallery/index'],'description' => 'Manage Galleries'],
            ['label' => 'Subscribers', 'url' => ['/subscriber/index'],'description' => 'Manage Subscribers'],
            ['label' => 'Instructors', 'url' => ['/instructor/index'],'description' => 'Manage Instructors'],
            ['label' => 'News And Events', 'url' => ['/news-and-event/index'],'description' => 'News And Events'],
            ['label' => 'Blocks', 'url' => ['/block/index'],'description' => 'Manage blocks'],
        ];

        return $this->render('index',[
            'links' => $links,
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionProfile($id)
    {
        $model = Admin::findOne($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->generateTokens();
                if ($model->save(false)) {
                    Yii::$app->session->setFlash('success', 'Profile has been successfully updated.');
                    return $this->redirect(['profile', 'id' => $model->id]);
                }
            }
        }

        return $this->render('profile', [
            'model' => $model,
        ]);

    }
}
