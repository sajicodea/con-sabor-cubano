<?php
return [
    'adminEmail' => 'tobin@codea.com',
    'supportEmail' => 'fitnessinfo@att.net',
    'blogTitle' => 'Blog',
    'blogTitleSeo' => 'Con-sabor-cubano - Blog',
    'blogFooter' => 'Copyright &copy; ' . date('Y') ,
    'blogPostPageCount' => '10',
    'blogLinks' => [
    ],
    'blogUploadPath' => 'upload/', //default to frontend/web/upload
];
