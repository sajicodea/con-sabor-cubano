<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'name' => 'Con-Sabor-Cubano',
    'components' => [
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_frontendUser',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                '/culture-and-history' => 'culture-and-history/index',
                '/instructors' => 'instructor/index',
                '/news-and-event' => 'news-and-event/index',
                '/studio-rental' => 'studio-rental/index',
                '/location' => 'site/location',
                '/contact' => 'site/contact',
                '/class-schedule' => 'class/schedule',
                '/salsa-lessons' => 'salsa-lessons/index',
                '/gallery' => 'gallery/index',
                '/register' => 'site/register',
                '/instructors' => 'class/instructors',
               // '/blog' => 'blog/index',
                '/class'=>'class/classes',
                '/checkout/<id>'=>'class/checkout',
                '/print'=>'class/print',
                '/class/<id>'=>'class/view',
                'testimonials' => 'testimonials/index',
                '/products' => 'class/products',
                'product-buy/<id>' => 'class/product-buy',
                'class-description' => 'class/class-description',
                'staff/<id>' => 'class/staff',
                '/<id>' => 'page/index',
            ],
        ],
        'session' => [
            'name' => '_frontendSessionId', // unique for frontend
        ],
        'urlManagerFrontEnd' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'http://con-sabor-cubano.codeaweb.net/',
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => [

            ],
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/user',
                'baseUrl' => '@web/themes/user',
                'pathMap' => [
                    '@app/views' => '@app/themes/user',
                ],
            ],
        ],
    ],
    'modules' => [
        'blog' => [
            'class' => 'funson86\blog\Module',
            'controllerNamespace' => 'funson86\blog\controllers\frontend'
        ],
    ],
    'params' => $params,
];
