<?php
namespace frontend\components;

use DevinCrossman\Mindbody\MB_API;
use linslin\yii2\curl;
use MindbodyAPI\MindbodyClient;
use yii\base\Exception;

class ClientMindBodyApi
{
    public $api;
    public $service;
    public $baseUrl;
    public $apiUrl;
    public $curl;
    public $error;

    public function __construct()
    {
        $this->api = new MB_API(array(
            "SourceName" => 'Toj',//'CONSABORCUBANODANCEFITNESS',
            "Password" => 'oJm78ZLouEHYZ5kQi6c8Qis7wdc=',//'UVMtj0xBRAAWEF3GS6rMvSzQGP8=',//'3jexP2kbkHetT1wm8A8M2ADczTI=',
            "SiteIDs" => array(37534)//37534
        ));

    }

    public function login($username, $password)
    {
        try {
            $api = $this->api;
            $validateLogin = $api->ValidateLogin(array(
                'Username' => $username,
                'Password' => $password
            ));
            if (!empty($validateLogin['ValidateLoginResult']['GUID'])) {
                return [
                    'key' => $validateLogin['ValidateLoginResult']['GUID'],
                    'client' => $validateLogin['ValidateLoginResult']['Client']
                ];
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function signup($data)
    {
        try {
            $api = $this->api;
            $options = array(
                'Clients' => array(
                    'Client' => $data,
                )
            );
            $signupData = $api->AddOrUpdateClients($options);
            if ($signupData && $signupData['AddOrUpdateClientsResult']['Clients']['Client']['Action'] == 'Added') {
                return [
                    'status' => 'success'
                ];
            } else {
                //$requiredFields = $api->GetRequiredClientFields();
                return [
                    'status' => 'error',
                    'data' => $signupData
                ];
            }
        } catch (Exception $error) {
            return [
                'status' => 'error',
                'message' => $error->getMessage()
            ];
        }
    }

    function sortClassesByDate($classes = array(), $type = 'StartDateTime')
    {
        $classesByDate = array();
        foreach ($classes as $class) {
            $classDate = date("Y-m-d", strtotime($class[$type]));
            if (!empty($classesByDate[$classDate])) {
                $classesByDate[$classDate] = array_merge($classesByDate[$classDate], array($class));
            } else {
                $classesByDate[$classDate] = array($class);
            }
        }
        ksort($classesByDate);
        foreach ($classesByDate as $classDate => &$classes) {
            usort($classes, function ($a, $b) use ($type) {
                if (strtotime($a[$type]) == strtotime($b[$type])) {
                    return 0;
                }
                return $a[$type] < $b[$type] ? -1 : 1;
            });
        }
        return $classesByDate;
    }

    function sortClassesByType($data = array(), $type = 'Name')
    {
        $classesByDate = array();
        foreach ($data as $class) {
            if (!empty($classesByDate[$type])) {
                $classesByDate[$type] = array_merge($classesByDate[$type], array($class));
            } else {
                $classesByDate[$type] = array($class);
            }
        }
        ksort($classesByDate);
        foreach ($classesByDate as &$data) {
            usort($data, function ($a, $b) use ($type) {
                if (strtotime($a[$type]) == strtotime($b[$type])) {
                    return 0;
                }
                return $a[$type] < $b[$type] ? -1 : 1;
            });
        }
        return $classesByDate;
    }

    public function post($api, $data = array())
    {
        try {

            return $data;
        } catch (Exception $error) {
            $this->error = $error;
            return false;
        }
    }

    public function getUniqueClass()
    {
        try {
            $classes = $this->classes();

            $classData = array();
            foreach ($classes as $classDate => $classes) {
                foreach ($classes as $class) {
                    $id = $class['ClassDescription']['ID'];
                    $classData[$id] = $class['ClassDescription']['Name'];
                }
            }
            return $classData;
        } catch (Exception $error) {
            return array();
        }

    }

    public function classes($startDate = '', $endDate = '', $program = '', $session = '', $staff_id = '')
    {
        try {
            $cache = \Yii::$app->cache;
            if (!$startDate) {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d', strtotime('today + 7 days'));
            }
            $key = 'api_classs_list_' . $startDate . '_' . $program . '_' . $session . '_' . $staff_id;
            $data = $cache->get($key);
            $data = false;
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $filter = array(
                    'StartDateTime' => $startDate,
                    'EndDateTime' => $endDate,
                    'HideCanceledClasses' => true,
                );
                if ($program) {
                    $filter['ProgramIDs'] = [$program];
                } else {
                    $programs = $this->getPrograms();
                    $programs = array_keys($programs);
                    $filter['ProgramIDs'] = $programs;
                }
                if ($session) {
                    $filter['SessionTypeIDs'] = [$session];
                } else {
                    $sessions = $this->getSessionTypes();
                    $sessions = array_keys($sessions);
                    $filter['SessionTypeIDs'] = $sessions;
                }

                if ($staff_id) {
                    $filter['StaffIDs'] = [$staff_id];
                }
                //print_r($filter);exit;
                $data = $api->GetClasses($filter);
                if (!empty($data['GetClassesResult']['Classes']['Class'])) {
                    $classes = $api->makeNumericArray($data['GetClassesResult']['Classes']['Class']);
                    $classes = $this->sortClassesByDate($classes);
                } else {
                    if (!empty($data['GetClassesResult']['Message'])) {
                        throw new Exception($data['GetClassesResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, null);
                $cache->set($key, $classes, 7200);

            } else {
                $classes = $cache->get($key);
            }
            return $classes;
        } catch (Exception $error) {
            return array();
        }

    }

    function getUpcomingClasses($ClassID)
    {
        try {
            $cache = \Yii::$app->cache;
            $key = 'api_upcoming_classs_list_' . $ClassID;
            $data = $cache->get($key);
            //$data = false;
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $programs = $this->getPrograms();
                $programs = array_keys($programs);
                $sessions = $this->getSessionTypes();
                $sessions = array_keys($sessions);
                $data = $api->GetClasses(array(
                    'StartDateTime' => date('Y-m-d'),
                    'EndDateTime' => date('Y') . '-12-31',
                    'ProgramIDs' => $programs,
                    'SessionTypeIDs' => $sessions,
                    'ClassDescriptionIDs' => [$ClassID],
                    'HideCanceledClasses' => true
                ));
                if (!empty($data['GetClassesResult']['Classes']['Class'])) {
                    $classes = $api->makeNumericArray($data['GetClassesResult']['Classes']['Class']);
                    $classes = $this->sortClassesByDate($classes);
                } else {
                    if (!empty($data['GetClassesResult']['Message'])) {
                        throw new Exception($data['GetClassesResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, null);
                $cache->set($key, $classes, 7200);
            } else {
                $classes = $cache->get($key);
            }
            return $classes;
        } catch (Exception $error) {
            return array();
        }
    }

    function getClassInfo($ClassID)
    {
        try {
            $cache = \Yii::$app->cache;
            $key = 'api_classs_description_' . $ClassID;
            $data = $cache->get($key);
            //$data = false;
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $data = $api->GetClassDescriptions(array(
                    'ClassDescriptionIDs' => [$ClassID],
                    'HideCanceledClasses' => true,
                    'StartClassDateTime' => date('Y-m-d'),
                    'EndClassDateTime' => date('Y') . '-12-31'
                ));
                if (!empty($data['GetClassDescriptionsResult']['ClassDescriptions']['ClassDescription'])) {
                    $classes = $data['GetClassDescriptionsResult']['ClassDescriptions']['ClassDescription'];
                } else {
                    if (!empty($data['GetClassDescriptionsResult']['Message'])) {
                        throw new Exception($data['GetClassDescriptionsResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, null);
                $cache->set($key, $classes, 7200);
            } else {
                $classes = $cache->get($key);
            }
            return $classes;
        } catch (Exception $error) {
            return array();
        }
    }

    function getClassDetails($ClassID)
    {
        try {
            $cache = \Yii::$app->cache;
            $key = 'api_classs_detail_' . $ClassID;
            $data = $cache->get($key);
            $data = false;
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $data = $api->GetClasses(array(
                    'ClassIDs' => [$ClassID],
                    'StartDateTime' => date('Y-m-d'),
                    'EndDateTime' => date('Y') . '-12-31',
                ));
                if (!empty($data['GetClassesResult']['Classes']['Class'])) {
                    $classes = $data['GetClassesResult']['Classes']['Class'];
                } else {
                    if (!empty($data['GetClassesResult']['Message'])) {
                        throw new Exception($data['GetClassesResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, null);
                $cache->set($key, $classes, 7200);
            } else {
                $classes = $cache->get($key);
            }
            return $classes;
        } catch (Exception $error) {
            return array();
        }
    }

    function getServiceDetails($ClassID)
    {
        try {
            $cache = \Yii::$app->cache;
            $key = 'api_classs_detail_' . $ClassID;
            $data = $cache->get($key);
            //$data = false;
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $filter = [
                    'ServiceIDs' => [$ClassID],
                    'LocationID' => 'All',
                    'HideRelatedPrograms' => true
                ];
                $data = $api->GetServices($filter);
                if (!empty($data['GetServicesResult']['Services']['Service'])) {
                    $services = $data['GetServicesResult']['Services']['Service'];
                } else {
                    if (!empty($data['GetServicesResult']['Message'])) {
                        throw new Exception($data['GetServicesResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, null);
                $cache->set($key, $services, 7200);
            } else {
                $services = $cache->get($key);
            }
            return $services;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getPrograms($type = 'DropIn')
    {
        try {
            $cache = \Yii::$app->cache;
            $key = 'api_service_list_' . $type;
            $data = $cache->get($key);
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $data = $api->GetPrograms(['ScheduleType' => $type, 'OnlineOnly' => 1]);
                $programs = array();
                if (!empty($data['GetProgramsResult']['Programs']['Program'])) {
                    $programsList = $api->makeNumericArray($data['GetProgramsResult']['Programs']['Program']);
                    foreach ($programsList as $program) {
                        $programs[$program['ID']] = $program['Name'];
                    }
                } else {
                    if (!empty($data['GetProgramsResult']['Message'])) {
                        throw new Exception($data['GetProgramsResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, $programs, 7200);
            } else {
                $programs = $cache->get($key);
            }
            return $programs;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getSessionTypes()
    {
        try {
            $cache = \Yii::$app->cache;
            $key = 'api_session_list';
            $data = $cache->get($key);
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $programs = $this->getPrograms();
                $programs = array_keys($programs);
                $data = $api->GetSessionTypes(['OnlineOnly' => 1, 'ProgramIDs' => $programs]);
                $sessions = array();
                if (!empty($data['GetSessionTypesResult']['SessionTypes']['SessionType'])) {
                    $sessionsList = $api->makeNumericArray($data['GetSessionTypesResult']['SessionTypes']['SessionType']);
                    foreach ($sessionsList as $session) {
                        $sessions[$session['ID']] = $session['Name'];
                    }
                } else {
                    if (!empty($data['GetSessionTypesResult']['Message'])) {
                        throw new Exception($data['GetSessionTypesResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, null);
                $cache->set($key, $sessions, 7200);
            } else {
                $sessions = $cache->get($key);
            }

            return $sessions;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getStaff()
    {
        /*$api = $this->api;
         $sessions = $this->getSessionTypes();
         $sessions = array_keys($sessions);
        $data = $api->GetStaff([
             'Filters' => [
                 'StaffFilter'=>['ClassInstructor']
             ]
         ]);
         print_r($data);*/
        return [
            '100000083' => 'Dawn Williams',
            '100000109' => 'Dalila S',
            '100000066' => 'Heidi J.',
            '100000092' => 'Ariel Flores',
           // '100000091' => 'JAY L.',
            '100000099' => 'Melissa S.',
            '100000117' => 'Francisco F',
            '100000068' => 'Michel R',
            '100000116' => 'Leyder C.',
            '100000061' => 'Willa WJ.',
            '100000118' => 'Uma M.',
            '100000101' => 'Royland L',
            '100000010' => 'Joanne Sanchez-Rosa',
            '100000115' => 'Anna Z.',
            '100000123' => 'JoAnn DJ',
        ];
    }

    public function addClientsToClass($id)
    {
        try {
            $api = $this->api;
            $userID = \Yii::$app->user->id;
            $data = $api->AddClientsToClasses([
                'ClassIDs' => [$id],
                'ClientIDs' => [$userID],
                //'Test' => true,
                'RequirePayment' => true
            ]);
            if (!empty($data['AddClientsToClassesResult']['Classes'])) {
                $class = $data['AddClientsToClassesResult']['Classes']['Class'];
            } else {
                if (!empty($data['AddClientsToClassesResult']['Message'])) {
                    throw new Exception($data['AddClientsToClassesResult']['Message']);
                } else {
                    throw new Exception('Error getting classes');
                }
            }
            return $class;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getClientAccountBalances($id)
    {
        try {
            $api = $this->api;
            $userID = \Yii::$app->user->id;
            $data = $api->GetClientAccountBalances([
                'ClassID' => $id,
                'ClientIDs' => [$userID],
                /*'Test' => true,
                'RequirePayment' => true*/
            ]);
            if (!empty($data['GetClientAccountBalancesResult']['Clients']['Client'])) {
                $user = $data['GetClientAccountBalancesResult']['Clients']['Client'];
            } else {
                if (!empty($data['GetClientAccountBalancesResult']['Message'])) {
                    throw new Exception($data['GetClientAccountBalancesResult']['Message']);
                } else {
                    throw new Exception('Error getting classes');
                }
            }
            return $user;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getServices($id = false)
    {
        try {
            $cache = \Yii::$app->cache;
            $key = 'api_service_list_' . $id;
            $data = $cache->get($key);
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $filter = [
                    'LocationID' => 'All',
                    'HideRelatedPrograms' => true,
                    'SellOnline' => true,
                ];
                if ($id) {
                    $filter['ProgramIDs'] = [$id];
                }
                $data = $api->GetServices($filter);
                if (!empty($data['GetServicesResult']['Services']['Service'])) {
                    $services = $data['GetServicesResult']['Services']['Service'];
                } else {
                    if (!empty($data['GetServicesResult']['Message'])) {
                        throw new Exception($data['GetServicesResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, null);
                $cache->set($key, $services, 7200);
            } else {
                $services = $cache->get($key);
            }
            return $services;
        } catch (Exception $error) {
            return array();
        }
    }

    public function purchaseClass($id, $paymentInfo)
    {
        try {
            $api = $this->api;
            $filter = [
                'ClientID' => \Yii::$app->user->id,
                //'Test' => false,
                'SellOnline' => 'true',
                'SendEmail' => true,
                'ClassID' => $id,
                'LocationID' => 'All',
                'HideRelatedPrograms' => true,
                'CartItems' => array(
                    'CartItem' => array(
                        'Quantity' => 1,
                        'Item' => new \SoapVar(
                            array('ID' => $id),
                            SOAP_ENC_ARRAY,
                            'Service',
                            $this->getApiNamespace()
                        ),
                        'DiscountAmount' => 0
                    )
                ),
                'Payments' => array(
                    'PaymentInfo' => new \SoapVar(
                        $paymentInfo,
                        SOAP_ENC_ARRAY,
                        'CreditCardInfo',
                        $this->getApiNamespace()
                    )
                )
                //'PromotionCode' => ''
            ];
            $response = $api->CheckoutShoppingCart($filter);
            if (isset($response['CheckoutShoppingCartResult']['ShoppingCart']['CartItems'])) {
                return ['status' => 'success', 'message' => 'Thank you for shopping in our online store.'];
            } elseif (isset($response['CheckoutShoppingCartResult']['Message'])) {
                throw new Exception(($response['CheckoutShoppingCartResult']['Message']));
            } else {
                throw new Exception('Something went wrong.Please try again later.');
            }
        } catch (\Exception $error) {
            return ['status' => 'error', 'message' => $error->getMessage()];
        }
    }

    public function getClientPurchases($id)
    {
        $api = $this->api;
        $filter = [
            'ClientID' => \Yii::$app->user->id,
            'ClassID' => $id
        ];

        $data = $api->GetClientServices($filter);
    }

    function toComplexType($item, $objectName)
    {
        return new \SoapVar($item, XSD_ANYTYPE, $objectName, $this->getApiNamespace());
    }

    function getApiNamespace()
    {
        return 'http://clients.mindbodyonline.com/api/0_5';
    }

    public function updateclient($data)
    {
        try {
            $api = $this->api;
            $updateData = $api->AddOrUpdateClients($data);
            if ($updateData && $updateData['AddOrUpdateClientsResult']['Clients']['Client']['Action'] == 'Updated') {
                return [
                    'status' => 'success'
                ];
            } elseif (isset($updateData['AddOrUpdateClientsResult']['Message'])) {
                throw new Exception(($updateData['CheckoutShoppingCartResult']['Message']));
            } else {
                throw new Exception('Something went wrong.Please try again later.');
            }
        } catch (Exception $error) {
            return [
                'status' => 'error',
                'message' => $error->getMessage()
            ];
        }
    }

    public function getClassDescriptions()
    {
        try {
            $cache = \Yii::$app->cache;
            $key = 'api_description_list_all';
            $data = $cache->get($key);
            //$data  =false;
            if ($data === false || count($data) <= 0) {
                $api = $this->api;
                $programs = $this->getPrograms();
                $programs = array_keys($programs);
                $data = $api->GetClassDescriptions(array(
                    //'ClassDescriptionIDs' => [$ClassID],
                    'HideCanceledClasses' => true,
                    'ScheduleType' => 'DropIn',
                    'ProgramIDs' => $programs,
                    'StartClassDateTime' => date('Y-m-d'),
                    'EndClassDateTime' => date('Y') . '-12-31'
                ));
                if (!empty($data['GetClassDescriptionsResult']['ClassDescriptions']['ClassDescription'])) {
                    $classes = $data['GetClassDescriptionsResult']['ClassDescriptions']['ClassDescription'];
                } else {
                    if (!empty($data['GetClassDescriptionsResult']['Message'])) {
                        throw new Exception($data['GetClassDescriptionsResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
                $cache->set($key, null);
                $cache->set($key, $classes, 7200);
            } else {
                $classes = $cache->get($key);
            }
            return $classes;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getInstructors()
    {
        try {
            $api = $this->api;
            $sessions = $this->getSessionTypes();
            $sessions = array_keys($sessions);
            $staff = $this->getStaff();
            $staff = array_keys($staff);
            $data = $api->GetStaff(array(
                //'ClassDescriptionIDs' => [$ClassID],
                //'HideCanceledClasses' => true,
               // 'StaffIDs' => $staff,
                //'SessionTypeIDs' => $sessions,
                'StartDateTime' => date('Y-m-d'),
               // 'Filters' => 'AppointmentInstructor'
                //'EndClassDateTime' => date('Y') . '-12-31'
            ));
            print_r($data);exit;
            if (!empty($data['GetStaffResult']['StaffMembers']['Staff'])) {
                $staffs = $data['GetStaffResult']['StaffMembers']['Staff'];
                foreach ($staffs as $key => $staff) {

                    if (!isset($staff['ID']) || $staff['ID'] < 0) {

                        unset($staffs[$key]);
                    } else {
                        $url = $this->getStaffImgURL($staff['ID']);
                    }
                }
            } else {
                if (!empty($data['GetStaffResult']['Message'])) {
                    throw new Exception($data['GetStaffResult']['Message']);
                } else {
                    throw new Exception('Error getting classes');
                }
            }
            return $staffs;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getStaffImgURL($id)
    {
        $api = $this->api;
        $filter = array(
            //'ClassDescriptionIDs' => [$ClassID],
            'StaffID' => $id,
        );
        $data = $api->GetStaffImgURL($filter);
        print_r($data);
    }

    public function getAStaff($id)
    {
        try {
            $api = $this->api;
            $sessions = $this->getSessionTypes();
            $sessions = array_keys($sessions);
            $data = $api->GetStaff(array(
                'HideCanceledClasses' => true,
                'StaffIDs' => [$id],
                'SessionTypeIDs' => $sessions,
                'StartDateTime' => date('Y-m-d'),
            ));
            if (!empty($data['GetStaffResult']['StaffMembers']['Staff'])) {
                $staff = $data['GetStaffResult']['StaffMembers']['Staff'];
            } else {
                if (!empty($data['GetStaffResult']['Message'])) {
                    throw new Exception($data['GetStaffResult']['Message']);
                } else {
                    throw new Exception('Error getting classes');
                }
            }
            return $staff;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getClassByStaff($id)
    {
        try {
            $api = $this->api;
            $cache = \Yii::$app->cache;
            $key = 'api_class_by_staff_' . $id;
            $data = $cache->get($key);
            if ($data === false || count($data) <= 0) {
                $end = date('Y-m-d', strtotime('+4 months'));
                $data = $api->GetClasses(array(
                    'StaffIDs' => [$id],
                    'StartDateTime' => date('Y-m-d'),
                    'EndDateTime' => $end,
                ));
                if (!empty($data['GetClassesResult']['Classes']['Class'])) {
                    $class = $data['GetClassesResult']['Classes']['Class'];
                    $classes = array();
                    foreach ($class as $classDetails) {
                        $classes[$classDetails['ClassDescription']['ID']] = $classDetails['ClassDescription'];
                    }
                } else {
                    if (!empty($data['GetClassesResult']['Message'])) {
                        throw new Exception($data['GetClassesResult']['Message']);
                    } else {
                        throw new Exception('Error getting classes');
                    }
                }
            } else {
                $classes = $cache->get($key);
            }
            return $classes;
        } catch (Exception $error) {
            return array();
        }
    }

    public function getAcceptedCardType ()
    {
        try {
            $api = $this->api;

            $data = $api->GetAcceptedCardType();
            print_r($data);exit;
            if (!empty($data['GetStaffResult']['StaffMembers']['Staff'])) {
                $staff = $data['GetStaffResult']['StaffMembers']['Staff'];
            } else {
                if (!empty($data['GetStaffResult']['Message'])) {
                    throw new Exception($data['GetStaffResult']['Message']);
                } else {
                    throw new Exception('Error getting classes');
                }
            }
            return $staff;
        } catch (Exception $error) {
            return array();
        }
    }
    public function getActivate()
    {
        try {
            $api = $this->api;

            $data = $api->GetActivationCode();
            print_r($data);exit;
        } catch (Exception $error) {
            return array();
        }
    }
}