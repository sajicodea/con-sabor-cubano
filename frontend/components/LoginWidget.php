<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\LoginForm;
use yii\helpers\Url;


class LoginWidget extends Widget
{

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $model = new LoginForm;

        print '<div class="input-success"></div>';
        print ' <h3>Login/ Signup</h3>';
        print '<div class="log-blk schedule-login reg-fm">';
            $form = \yii\widgets\ActiveForm::begin([
                'id' => 'login-client-form-wdgt',
                'action' => Url::to(['site/client-login']),
                'enableAjaxValidation' => true,
                'validationUrl' => Url::to(['site/ajax-client-login']),
            ]);
            print '<div class="col-md-4">';
                print $form->field($model, 'username')->textInput(['class' => 'form-control txtbox']);
            print '</div>';
            print '<div class="col-md-4">';
                print $form->field($model, 'password')->passwordInput(['class' => 'form-control txtbox']);
            print '</div>';
            print '<div class="col-md-4">';
                print Html::submitInput('Login', ['class' => 'btn btn-default sbmt']);
               print '<label>';
                    print'<span>';
                        print'<a href="'. \yii\helpers\Url::to(['site/register']).'">Sign up Today!</a>';
                    print '</span>';
                print '</label>';
            print '</div>';
        print '<div class="clearfix"></div>';
        $form->end();
        print '</div>';


    }

} 