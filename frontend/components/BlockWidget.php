<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use backend\models\Block;
use yii\helpers\Url;




class BlockWidget extends Widget{
    
    public $block_keys = array();
    
    public function init(){
        parent::init();
    }
    
    public function run() {
           
        if($this->block_keys){
            $query = "SELECT block_key,title,content FROM {{%block}} WHERE block_key IN ('".implode("', '",  $this->block_keys)."') AND status=1";
            $blocks = \Yii::$app->db->createCommand($query)->queryAll();//->cache(3600)
            if($blocks){
                foreach($blocks as $block){
                    print $block['content'];
                }
            }
        }
    }
    
} 