<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;
use common\models\Subscriber;
use yii\helpers\Url;



class SubscribeWidget extends Widget{
    
    public function init(){
        parent::init();
    }
    public function run() {
        $model = new Subscriber;
        $form = \yii\widgets\ActiveForm::begin([
            'id' => 'subscriber-footer-form',
            'options' => ['class' => 'form-inline'],
            'action' => Url::to(['site/subscribe']),
            'validationUrl' => Url::to(['site/ajax-subscribe']),
            'enableAjaxValidation' => true,
            'validateOnSubmit' => true,
        ]);
        print '<div class="input-success"></div>';
        //print '<div class="input-group subscribe-form-blk">';
        print $form->field($model, 'email')->textInput(['placeholder'=>'Enter your email'])->label(false);
        //print '<div class="input-group-btn">';
        print Html::submitInput('SUBMIT',['class'=>'btn btn-default']);
        //print '</div>';
        //print '</div>';
        $form->end();       
    }
    
} 