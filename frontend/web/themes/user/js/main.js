
//sticky header

$(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
        $('header').addClass("sticky");
    }
    else{
        $('header').removeClass("sticky");
    }
});

$(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
        $('.dropdown-menu').addClass("list-menu");
    }
    else{
        $('.dropdown-menu').removeClass("list-menu");
    }
});


$(document).ready(function() {
 
  var owl = $("#owl-slide1");
 
  owl.owlCarousel({
    navigation : true,
    singleItem : true,
    transitionStyle : "fadeUp",
	autoPlay:true,
	pagination:true
  });
 
});


$(document).ready(function() {
 
  var owl = $("#owl-what_do");
 
  owl.owlCarousel({
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1000,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,2], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : true,  // itemsMobile disabled - inherit from itemsTablet option
	  autoPlay:true,
	  navigation : true,
      pagination:false
  });
 
});


$(document).ready(function() {
 
  $("#owl-testimonials").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
	  autoPlay:true,
      pagination:false,
	  autoHeight : true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
 
});


//fadethis
$(document).ready(function() {
	$(window).fadeThis({
		speed: 1000,
	});
});



$(document).ready(function(){/* google maps -----------------------------------------------------*/
//google.maps.event.addDomListener(window, 'load', initialize);

function initialize() {

  /* position Amsterdam */
  var latlng = new google.maps.LatLng(52.3731, 4.8922);

  var mapOptions = {
    center: latlng,
    scrollWheel: false,
    zoom: 13
  };
  
  var marker = new google.maps.Marker({
    position: latlng,
    url: '/',
    animation: google.maps.Animation.DROP
  });
  
  var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
  marker.setMap(map);

};
/* end google maps -----------------------------------------------------*/
});