$(document).on('click', '.showModalButton', function () {
    //check if the modal is open. if it's open just reload content not whole modal
    //also this allows you to nest buttons inside of modals to reload the content it is in
    //the if else are intentionally separated instead of put into a function to get the
    //button since it is using a class not an #id so there are many of them and we need
    //to ensure we get the right button and content.
    $('#modal').find('#modalContent').html('');
    if ($('#modal').data('bs.modal').isShown) {
        $('#modal').find('#modalContent')
            .load($(this).attr('value'));
        if ($('#modal #modalHeader h4').length > 0) {
            $('#modal #modalHeader h4').html($(this).attr('title'));
        } else {
            $('#modal #modalHeader').append('<h4>' + $(this).attr('title') + '</h4>');
        }
        //dynamiclly set the header for the modal via title tag
        //document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
    } else {
        //if modal isn't open; open it and load content
        $('#modal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
        if ($('#modal #modalHeader h4').length > 0) {
            $('#modal #modalHeader h4').html($(this).attr('title'));
        } else {
            $('#modal #modalHeader').append('<h4>' + $(this).attr('title') + '</h4>');
        }

        //dynamiclly set the header for the modal via title tag
        //document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
    }
});

$('body').on('beforeValidate', '#register-client-form, #login-client-form, ' +
    '#ajax-activate-client-form,#client-password-reset-form,' +
    '#ajax-reset-password-form, #subscriber-footer-form', function (response) {
    var form = $(this);
    form.find('.input-success').html('');
    beforeLoading();
});
$('body').on('afterValidate', '#register-client-form, #login-client-form, ' +
    '#ajax-activate-client-form,#client-password-reset-form,' +
    '#ajax-reset-password-form', function (response) {
    var form = $(this);
    form.find('button').prop('disabled',false);
    afterLoading();
});
$('body').on('beforeSubmit', '#register-client-form', function (response) {
    var form = $(this);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
        return false;
    }

    clientRegisterForm(form);
    return false;
});
$('body').on('beforeSubmit', '#ajax-activate-client-form', function (response) {
    var form = $(this);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
        return false;
    }

    clientActivateForm(form);
    return false;
});
$('body').on('beforeSubmit', '#client-password-reset-form', function (response) {
    var form = $(this);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
        return false;
    }

    clientResetPasswordForm(form);
    return false;
});
$('body').on('beforeSubmit', '#ajax-reset-password-form', function (response) {
    var form = $(this);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
        return false;
    }

    clientResetPasswordForm(form);
    return false;
});
$('body').on('beforeSubmit', '#ajax-recharge-form', function (response) {
    var form = $(this);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
        return false;
    }

    rechargeForm(form);
    return false;
});
$('body').on('beforeSubmit', '#subscriber-footer-form', function (response) {
    var form = $(this);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
        return false;
    }

    submitSubscribeForm(form);
    return false;
});
function clientRegisterForm(form) {
    onJsonAjaxSubmit(
        form.attr('action'),
        'post',
        form.serialize(),
        onSuccess,
        onError
    )
    function onSuccess(response) {
        afterLoading();
        if (response.status == 'success') {
            form.find('div.input-success').html(response.message);
            form.find('div#signup-content').html('');
        }
    }

    return false;
}

function clientActivateForm(form) {
    onJsonAjaxSubmit(
        form.attr('action'),
        'post',
        form.serialize(),
        onSuccess,
        onError
    )
    function onSuccess(response) {
        afterLoading();
        if (response.status == 'success' || response.status == 'error') {
            form.find('div.input-success').html(response.message);
            form.find('div#activate-content').html('');
            setTimeout(function () {
                window.location.href = baseUrl;
            }, 2000);
        }
    }

    return false;
}

function clientResetPasswordForm(form) {
    onJsonAjaxSubmit(
        form.attr('action'),
        'post',
        form.serialize(),
        onSuccess,
        onError
    )
    function onSuccess(response) {
        afterLoading();
        if (response.status == 'success' || response.status == 'error') {
            form.find('div.input-success').html(response.message);
            form.find('div#activate-content').html('');
            setTimeout(function () {
                window.location.href = baseUrl;
            }, 4000);
        } else {
            $('#modal').find('#modalContent').html(response);
        }
    }

    return false;
}

function onError(response) {
    afterLoading();
    console.log(response);
    $('.ajax-loading').removeClass('active');
}
function onAjaxSubmit(url, type, data, onSuccess, onError) {
    $.ajax({
        url: url,
        type: type,
        data: data,
        success: onSuccess,
        beforeSend: beforeLoading(),
        onError: onError
    });
}

function onJsonAjaxSubmit(url, type, data, onSuccess, onError) {
    $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: 'json',
        beforeSend: beforeLoading(),
        success: onSuccess,
        onError: onError
    });
}
function onSuccess() {
    afterLoading();
}
function beforeLoading()
{
    //$('body').spin(true);
    $('#modal').find('#modalContent button').prop('disabled',true);
}
function afterLoading()
{
    //$('body').spin(false);
}
function submitSubscribeForm(form)
{
    onJsonAjaxSubmit(
        form.attr('action'),
        'post',
        form.serialize(),
        onSuccess,
        onError
    )
    function onSuccess(response) {
        if(response.code == 100) {
            form.find('div.input-success').html(response.message);
            $('#subscriber-email').val('');
        }
    }

    return false;
}