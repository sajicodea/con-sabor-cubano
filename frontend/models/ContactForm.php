<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $body;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, phone and body are required
            [['name', 'email', 'phone', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
            'body' => 'Message'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        $body  = 'Hello,<br /><br />';
        $body .= '<table border="1" cellpadding="10">';
        $body .= '<tr><td>Name</td><td>:</td><td>'.$this->name.'</td></tr>';
        $body .= '<tr><td>Email Address</td><td>:</td><td>'.$this->email.'</td></tr>';
        $body .= '<tr><td>Phone</td><td>:</td><td>'.$this->phone.'</td></tr>';
        $body .= '<tr><td>Message</td><td>:</td><td>'.$this->body.'</td></tr>';

        $body .= '</table>';
        $body .= "<br /><br />Thank you,<br />".Yii::$app->name;

        $body_text  = "Hello,\n\n";
        $body_text .= "Name:".$this->name."\n\n";
        $body_text .= "Email Address:".$this->email."\n\n";
        $body_text .= "Message:".$this->body."\n\n";
        $body_text .= "Thank you,\n".Yii::$app->name;

        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([\Yii::$app->params['supportEmail'] => 'CON SABOR CUBANO'])
            ->setSubject('Contact Us : '.Yii::$app->name)
            ->setTextBody($body_text)
            ->setHtmlBody($body)
            ->send();
        return true;
    }
}
