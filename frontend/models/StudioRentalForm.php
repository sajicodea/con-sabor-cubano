<?php
namespace frontend\models;

use Yii;
use frontend\components\ClientMindBodyApi;
use kartik\password\StrengthValidator;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class StudioRentalForm extends Model
{
    public $full_name;
    public $email;
    public $phone;
    public $event_description;
    public $no_attendees;
    public $requested_date;
    public $requested_time;
    public $about_us;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name','email'],'required'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['no_attendees','number'],
            [['requested_date'], 'date','format' => 'php:Y-m-d'],
            ['email', 'string', 'max' => 255],

            [['full_name','email','phone','event_description','no_attendees','requested_date','requested_time','about_us'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'event_description' => 'Describe Event & Space Needs',
            'no_attendees' => 'Anticipated Number of Attendees',
            'requested_time' => 'Requested Time(s) for Event',
            'about_us' => 'How Did You Hear About Us?',
            'requested_date' => 'Requested Date(s) for Event',
        ];
    }

    public function sendEmail($email)
    {
        $body  = 'Hello,<br /><br />';
        $body .= '<table border="1" cellpadding="10">';
        $body .= '<tr><td>Name</td><td>:</td><td>'.$this->full_name.'</td></tr>';
        $body .= '<tr><td>Phone</td><td>:</td><td>'.$this->phone.'</td></tr>';
        $body .= '<tr><td>Email Address</td><td>:</td><td>'.$this->email.'</td></tr>';
        $body .= '<tr><td>Describe Event & Space Need</td><td>:</td><td>'.$this->event_description.'</td></tr>';
        $body .= '<tr><td>Anticipated Number of Attendees</td><td>:</td><td>'.$this->no_attendees.'</td></tr>';
        $body .= '<tr><td>Requested Date(s) for Event</td><td>:</td><td>'.$this->requested_date.'</td></tr>';
        $body .= '<tr><td>Requested Time(s) for Event</td><td>:</td><td>'.$this->requested_time.'</td></tr>';
        $body .= '<tr><td>How Did You Hear About Us?</td><td>:</td><td>'.$this->about_us.'</td></tr>';

        $body .= '</table>';
        $body .= "<br /><br />Thank you,<br />".Yii::$app->name;

        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([\Yii::$app->params['supportEmail'] => 'CON SABOR CUBANO'])
            ->setSubject('Studio Rental Request  : '.Yii::$app->name)
            //->setTextBody($body_text)
            ->setHtmlBody($body)
            ->send();
        return true;
    }
}
