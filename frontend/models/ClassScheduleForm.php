<?php
namespace frontend\models;

use Yii;
use frontend\components\ClientMindBodyApi;
use kartik\password\StrengthValidator;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class ClassScheduleForm extends Model
{
    public $staff_id;
    public $program;
    public $session_type;
    public $date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_id','session_type','program','date'],'safe'],
        ];
    }
}
