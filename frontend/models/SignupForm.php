<?php
namespace frontend\models;

use frontend\components\ClientMindBodyApi;
use kartik\password\StrengthValidator;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $Username;
    public $Email;
    public $Password;
    public $PasswordRepeat;
    public $FirstName;
    public $LastName;
    public $AddressLine1;
    public $City;
    public $State;
    public $Country;
    public $EmailOptIn;
    public $PromotionalEmailOptIn;
    public $MobilePhone;
    public $PostalCode;
    public $ReferredBy;
    public $IsAgree;
    public $BirthDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'Username',
                    'Password',
                    'PasswordRepeat',
                    'Email',
                    'FirstName',
                    'LastName',
                    'State',
                    'Country',
                    //'EmailOptIn',
                    //'PromotionalEmailOptIn',
                    'MobilePhone',
                    'IsAgree',
                    'AddressLine1',
                    'PostalCode',
                    'City',
                    'ReferredBy',
                    'BirthDate'
                ],
                'required',
                'on' => 'register'
            ],
            ['IsAgree','checkAgree'],
            ['Email', 'filter', 'filter' => 'trim'],
            ['Email', 'required'],
            ['Email', 'email'],
            ['Email', 'string', 'max' => 255],

            ['Password', 'compare', 'compareAttribute'=>'PasswordRepeat', 'message' => 'Password Repeat must be equal to password'],
            [['PasswordRepeat'], StrengthValidator::className(), 'min'=>8, 'digit'=>0, 'special'=>1,'upper' => 1,'lower' => 1,'hasUser' => false,
                'hasEmail' => false],
            ['Password', 'string', 'min' => 6],
            [
                [
                    'Username',
                    'Password',
                    'PasswordRepeat',
                    'Email',
                    'FirstName',
                    'LastName',
                    'AddressLine1',
                    'City',
                    'State',
                    'Country',
                    'EmailOptIn',
                    'PromotionalEmailOptIn',
                    'MobilePhone',
                    'PostalCode',
                    'IsAgree',
                    'BirthDate'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Password' => 'Password Repeat',
            'PasswordRepeat' => 'Password',
            'PostalCode' => 'Zip',
            'AddressLine1' => 'Address',
            'EmailOptIn' => 'Email Reminder Opt In',
            'PromotionalEmailOptIn' => 'Promotional Email Opt In',
            'IsAgree' => 'I agree'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $api = new ClientMindBodyApi();
        $data = $this->attributes;
        unset($data['PasswordRepeat']);
        foreach($data as $key => $item) {
            if(!$item) {
                unset($data[$key]);
            }
        }
        $data = $api->signup($data);
        if ($data['status'] == 'error') {
            $requiredFields = $data['data'];
            $data = $requiredFields['AddOrUpdateClientsResult']['Clients']['Client']['Messages']['string'];
            if(is_array($data)) {
                foreach($requiredFields['AddOrUpdateClientsResult']['Clients']['Client']['Messages']['string'] as $details) {
                    $errors = explode(' - ',$details);
                    if(count($errors) > 1) {
                        $this->addError($errors[0], $errors[1]);
                    }
                }
            } else {
                $errors = explode(' - ',$data);
                if(count($errors) > 1) {
                    $this->addError($errors[0], $errors[1]);
                }
            }

            return false;
        } else {
            return true;
        }
    }
    public function referralType()
    {
        return [
            'Another Client' => 'Another Client',
            'Newspaper' => 'Newspaper',
            'Radio' => 'Radio',
            'Flyer'=>'Flyer',
            'Internet' => 'Internet',
            'Yellow Pages'=>'Yellow Pages',
            'Keila N.'=>'Keila N.',
            'Enrique D.'=>'Enrique D.'
        ];
    }
    public function checkAgree()
    {
        if(!$this->IsAgree) {
            $this->addError('IsAgree','You must agree to the studio liability release!');
        }
    }
}
