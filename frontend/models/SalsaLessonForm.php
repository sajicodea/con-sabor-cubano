<?php
namespace frontend\models;

use Yii;
use frontend\components\ClientMindBodyApi;
use kartik\password\StrengthValidator;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SalsaLessonForm extends Model
{
    public $first_name;
    public $last_name;
    public $email;
    public $schedule;
    public $age_group;
    public $gender;
    public $is_new;
    public $contact;
    public $experience;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name','last_name','email'],'required'],
            ['email','email'],
            [['first_name','last_name'], 'string', 'max' => 255],

            [['first_name','last_name','schedule','age_group','gender','is_new','contact','experience'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'schedule' => 'When are you available to come in the evening? or What\'s you available schedule?',
            'is_new' => 'New to Salsa Class?',
            'experience' => 'or how much experience do you have?',
            'contact' => 'Best contact Information',
        ];
    }

    public function sendEmail($email)
    {
       if(count($this->schedule) > 0) {
           $this->schedule = $this->schedule[0];
       }
        if(count($this->is_new) > 0) {
            $this->is_new = $this->is_new[0];
        }
        $body  = 'Hello,<br /><br />';
        $body .= '<table border="1" cellpadding="10">';
        $body .= '<tr><td>First Name</td><td>:</td><td>'.$this->first_name.'</td></tr>';
        $body .= '<tr><td>Last Name</td><td>:</td><td>'.$this->last_name.'</td></tr>';
        $body .= '<tr><td>Email Address</td><td>:</td><td>'.$this->email.'</td></tr>';
        $body .= '<tr><td>When are you available to come in the evening? or What\'s you available schedule?</td><td>:</td><td>'.$this->schedule.'</td></tr>';
        $body .= '<tr><td>New to Salsa Class?</td><td>:</td><td>'.$this->is_new.'</td></tr>';
        if(!$this->is_new) {
            $body .= '<tr><td>how much experience do you have?</td><td>:</td><td>'.$this->experience.'</td></tr>';
        }
        $body .= '<tr><td>Best contact Information</td><td>:</td><td>'.$this->contact.'</td></tr>';
        $body .= '<tr><td>Gender</td><td>:</td><td>'.$this->gender.'</td></tr>';
        $body .= '<tr><td>Age group</td><td>:</td><td>'.$this->age_group.'</td></tr>';

        $body .= '</table>';
        $body .= "<br /><br />Thank you,<br />".Yii::$app->name;

        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([\Yii::$app->params['supportEmail'] => 'CON SABOR CUBANO'])
            ->setSubject('Salsa Lesson Request  : '.Yii::$app->name)
            //->setTextBody($body_text)
            ->setHtmlBody($body)
            ->send();
        return true;
    }
}
