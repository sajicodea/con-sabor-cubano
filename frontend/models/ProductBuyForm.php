<?php
namespace frontend\models;

use Yii;
use frontend\components\ClientMindBodyApi;
use kartik\password\StrengthValidator;
use yii\base\Model;

/**
 * Signup form
 */
class ProductBuyForm extends Model
{
    public $name;
    public $address;
    public $city;
    public $state;
    public $country;
    public $zip;
    public $card_no;
    public $expiry_year;
    public $expiry_month;
    public $save_info;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','address','city','state','country','zip','card_no','expiry_year','expiry_month'],'required'],

            [['name','address','city','state','country','zip','card_no','expiry_year','expiry_month','save_info'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name on card',
            'card_no' => 'Credit Card Number',
            'save_info' => 'Save my billing info into my account'
        ];
    }
}
