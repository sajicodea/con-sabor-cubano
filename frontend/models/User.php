<?php

namespace frontend\models;

use frontend\components\ClientMindBodyApi;
use frontend\components\VSPortalWebAPI;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $client;
    protected static $user;

    public static function login($username, $password)
    {
        $api = new ClientMindBodyApi();
        $user = $api->login($username, $password);
        if($user) {
            $authKey = $user['key'];
            self::$user =  [
                'id' => $user['client']['ID'],
                //'client' => $user['client'],
                'username' => $username,
                'password' => '',
                'authKey' => $authKey,
                'accessToken' => self::generateAccessToken(),
            ];
            $authData['key'] = $authKey;
            $authData['username'] = $username;
            \Yii::$app->session->set('access-auth-data',$authData);

            return new static (self::$user);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if(\Yii::$app->session->has('access-auth-data')) {
            $authData = \Yii::$app->session->get('access-auth-data');
            //\Yii::$app->session->remove('access-auth-data');
        } else {
            $authData = false;
        }

        return $id ? new static([
            'id' => $id,
            'authKey' => isset($authData['key'])?$authData['key']:'',
            'username' => isset($authData['username'])?$authData['username']:'',
        ]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        if (self::$user['accessToken'] === $token) {
                return new static(self::$user);

        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        if (strcasecmp(self::$user['username'], $username) === 0) {
            return new static(self::$user);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    /**
     * Generates "remember me" authentication key
     */
    public static function generateAccessToken() {
        return \Yii::$app->security->generateRandomString(). '_' . time();
    }
}
