<?php

namespace frontend\controllers;

use Yii;
use common\models\Testimonial;

class TestimonialsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new Testimonial();
        $testimonials = Testimonial::find()->where(['status' => 1])->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success',
                    'Your testimonial is waiting for admin approval.Thank you for contacting us.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        }

        return $this->render('index', [
            'model' => $model,
            'testimonials' => $testimonials
        ]);
    }

}
