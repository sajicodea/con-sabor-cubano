<?php

namespace frontend\controllers;

use Yii;
use common\models\NewsAndEvent;
use backend\Models\NewsAndEventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * NewsAndEventController implements the CRUD actions for NewsAndEvent model.
 */
class NewsAndEventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsAndEvent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $news = NewsAndEvent::find()->orderBy('id desc')->where(['status' => 1])->all();

        return $this->render('index', [
            'news' => $news,
        ]);
    }

    /**
     * Finds the NewsAndEvent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsAndEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsAndEvent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
