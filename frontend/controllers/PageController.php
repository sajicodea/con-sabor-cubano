<?php

namespace frontend\controllers;

use common\models\Page;
use yii\web\NotFoundHttpException;

class PageController extends \yii\web\Controller
{
    public $pageSlug;
    
    public function actionIndex($id)
    {
        $model = Page::findOne(['page_slug' => $id]);
        if(!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        $this->pageSlug = $id;
        
        return $this->render('index',[
            'model' => $model,
        ]);
    }

}
