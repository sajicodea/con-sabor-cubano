<?php

namespace frontend\controllers;

use common\models\Gallery;

class GalleryController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $galleries = Gallery::find()->orderBy('id desc')->all();

        return $this->render('index', [
            'galleries' => $galleries
        ]);
    }

}
