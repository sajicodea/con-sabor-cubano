<?php

namespace frontend\controllers;

use frontend\models\Country;
use frontend\models\ProductBuyForm;
use Yii;
use frontend\components\ClientMindBodyApi;
use frontend\models\ClassScheduleForm;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ClassController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['checkouts'],
                'rules' => [
                    [
                        'actions' => ['checkouts'],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSchedule()
    {
        return $this->render('schedule');
    }

    public function actionClasses()
    {
        $mindbody = new ClientMindBodyApi();
        $classes = $mindbody->getUniqueClass();

        return $this->render('class-list', [
            'classes' => $classes
        ]);
    }

    public function actionView($id)
    {
        $api = new ClientMindBodyApi();
        //$classes = $mindbody->getClassInfo($id);
        $classes = $api->getUpcomingClasses($id);
        $description = $api->getClassInfo($id);
        return $this->render('class-view', [
            'classes' => $classes,
            'description' => $description,
            'api' => $api
        ]);
    }

    public function actionCheckout($id)
    {
        if (\Yii::$app->user->isGuest) {
            \Yii::$app->session->set('checkout_class', $id);
            $this->redirect(['site/login']);
        } else {
            $api = new ClientMindBodyApi();
            $user = $api->getClientAccountBalances($id);
            $response = array();
            if (\Yii::$app->session->has('checkout_class')) {
                \Yii::$app->session->set('checkout_class', null);
            }
            $class = $api->getClassDetails($id);
            if (isset($_GET['confirm'])) {
                $confirm = true;
                if (count($class) <= 0) {
                    return $this->redirect(['/class']);
                }
            } else {
                $confirm = false;
                $response = $api->AddClientsToClass($id);
            }
            $programs = $api->getPrograms('All');
            return $this->render('checkout', [
                'user' => $user,
                'confirm' => $confirm,
                'id' => $id,
                'class' => $class,
                'response' => $response,
                'programs' => $programs,
                'api' => $api
            ]);
        }

    }

    public function actionProductBuy($id)
    {
        if (\Yii::$app->user->isGuest) {
            \Yii::$app->session->set('checkout_product', $id);
            $this->redirect(['site/login']);
        } else {
            $api = new ClientMindBodyApi();
            $country = Country::find()->select(['name', 'iso_code_2'])->all();
            $model = new ProductBuyForm();
            $service = $api->getServiceDetails($id);
            $response = false;
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                //print_r($model->save_info);exit;
                $amount = number_format($service['Price'], 2, '.', false);
                $paymentInfo = [
                    'CreditCardNumber' => $model->card_no,
                    'ExpYear' => $model->expiry_year,
                    'ExpMonth' => $model->expiry_month,
                    'Amount' => $amount,
                    'BillingCity' => $model->city,
                    'BillingState' => $model->state,
                    'BillingName' => $model->name,
                    'BillingAddress' => $model->address,
                    'BillingPostalCode' => $model->zip
                ];

                $response = $api->purchaseClass($id, $paymentInfo);
                Yii::$app->session->setFlash($response['status'], $response['message']);
                if ($response['status'] == 'success') {
                    $clientInfo = [
                        'UpdateAction' => 'Update',
                        'Clients' => [
                            'Client' => [
                                'ID' => Yii::$app->user->id,
                                'ClientCreditCard' => [
                                    'CardNumber' => $model->card_no,
                                    'ExpYear' => $model->expiry_year,
                                    'ExpMonth' => $model->expiry_month,
                                    'City' => $model->city,
                                    'State' => $model->state,
                                    'CardHolder' => $model->name,
                                    'Address' => $model->address,
                                    'PostalCode' => $model->zip
                                ]
                            ]
                        ]
                    ];
                    $response = $api->updateclient($clientInfo);
                    if ($response['status'] == 'error') {
                        Yii::$app->session->setFlash($response['status'], $response['message']);
                    }
                }

            }
            if (\Yii::$app->session->has('checkout_product')) {
                \Yii::$app->session->set('checkout_product', null);
            }
            if (count($service) <= 0) {
                return $this->redirect(['/products']);
            }

            return $this->render('product-buy', [
                'id' => $id,
                'service' => $service,
                'model' => $model,
                'country' => $country,
                'response' => $response
            ]);
        }
    }

    public function actionProducts()
    {
        $api = new ClientMindBodyApi();
        $programs = $api->getPrograms('All');
        //$api->getServices();
        return $this->render('product', [
            'programs' => $programs,
            'api' => $api
        ]);
    }

    public function actionClassDescription()
    {
        return $this->render('class-description');
    }

    public function actionStaff($id)
    {
        $api = new ClientMindBodyApi();
        $classes = $api->getClassByStaff($id);
        $staff = $api->getAStaff($id);
        if (!isset($staff['ID'])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('view-staff', [
            'staff' => $staff,
            'classes' => $classes
        ]);
    }

    public function actionPrint()
    {
        $this->layout = false;
        return $this->render('print');
    }

    public function actionInstructors()
    {
        return $this->render('instructors');
    }
}
