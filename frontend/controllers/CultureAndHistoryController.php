<?php

namespace frontend\controllers;

use Yii;
use common\models\CultureAndHistory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CultureAndHistoryController implements the CRUD actions for CultureAndHistory model.
 */
class CultureAndHistoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CultureAndHistory models.
     * @return mixed
     */
    public function actionIndex()
    {

        $data = CultureAndHistory::find()->where(['status' => 1])->all();

        return $this->render('index', [
            'data' => $data,
        ]);
    }

    /**
     * Finds the CultureAndHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CultureAndHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CultureAndHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
