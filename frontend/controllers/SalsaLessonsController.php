<?php

namespace frontend\controllers;

use Yii;
use frontend\models\SalsaLessonForm;

class SalsaLessonsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new SalsaLessonForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success',
                    'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

}
