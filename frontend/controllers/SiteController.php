<?php
namespace frontend\controllers;

use common\models\Testimonial;
use frontend\components\ClientMindBodyApi;
use frontend\models\Country;
use frontend\models\SalsaLessonForm;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','register','client-login'],
                'rules' => [
                    [
                        'actions' => ['signup','register','client-login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $testimonials = Testimonial::find()->where(['status' => 1])->limit(3)->all();
        return $this->render('index',[
            'testimonials' => $testimonials
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->redirect(['site/index?type=login']);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success',
                    'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionLocation()
    {
        return $this->render('location');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionClientLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if(Yii::$app->session->has('checkout_class')) {
                return $this->redirect(['/checkout/'.Yii::$app->session->get('checkout_class').'?confirm=1']);
            } elseif(Yii::$app->session->has('checkout_product')) {
                return $this->redirect(['/product-buy/'.Yii::$app->session->get('checkout_product')]);
            }
            return $this->goBack();
        }
        return $this->renderAjax('client-login', [
            'model' => $model,
        ]);

    }

    /***
     * Signup validation
     */
    public function actionAjaxClientLogin()
    {
        $model = new LoginForm();
        $model->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function actionRegister()
    {
        $model = new SignupForm();
        $country = Country::find()->select(['name','iso_code_2'])->all();
        $model->scenario = 'register';
        $errors = false;
        if ($model->load(Yii::$app->request->post())) {
            if($model->BirthDate) {
                $model->BirthDate = date('Y-m-d',strtotime($model->BirthDate));
            }
            $user = $model->signup();
            if ($user) {
                $login = new LoginForm();
                $login->username = $model->Username;
                $login->password = $model->Password;
                $login->login();

                return $this->redirect(['site/index']);
            }
        }

        return $this->render('client-register', [
            'model' => $model,
            'errors' => $errors,
            'country' => $country
        ]);
    }

    /***
     * Signup validation
     */
    public function actionAjaxRegister()
    {
        $model = new SignupForm();
        $model->scenario = 'register';
        $model->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function actionRequest()
    {
        $api = new ClientMindBodyApi();
        $classes = $api->getActivate();;//26663
        //$classes = $api->purchaseClass(10158,50);
        //$classes = $api->getClassDetails(28317);
        //$response = $api->AddClientsToClass(28317);
        print_r($classes);
    }

    public function actionCheckEmail()
    {

        /*$sals = new SalsaLessonForm();
        echo Yii::$app->params['supportEmail'];
        $ap = $sals->sendEmail('tobin@codeatech.com');
        var_dump($ap);*/
    }
    /***
     * Subscriber validation
     */
    public function actionAjaxSubscribe()
    {
        $model = new \common\models\Subscriber;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
        {
            Yii::$app->response->format = 'json';
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    /***
     * Subscriber validation
     */
    public function actionSubscribe()
    {
        $model = new \common\models\Subscriber;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
        {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if(!$model->save()){
                throw new NotAcceptableHttpException('Something went wrong.Please try again.');
            }
            return [
                'message' => '<div class="alert alert-success">Thank you for subscribing with us.
                                    Please contact us if you have any queries.</div>',
                'code' => 100,
            ];
        }
    }

    public function actionHealcode()
    {
        return $this->render('healcode');
    }

    public function actionSchedule()
    {
        return $this->render('schedule');
    }

    public function actionClassDescription()
    {
        return $this->render('class-description');
    }

    public function actionInstructors()
    {
        return $this->render('instructors');
    }

}
