<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UserAsset extends AssetBundle
{
    public $sourcePath = '@app/web/themes/user';
    public $css = [
        'css/font-awesome.min.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/owl.transitions.css',
        'css/main.css',
    ];
    public $js = [
        'js/main.js',
        'js/vendor/modernizr-2.8.3-respond-1.4.2.min.js',
        'js/plugins.js',
        'js/custom.js',
        'js/owl.carousel.js',
        'js/jquery.fadethis.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
