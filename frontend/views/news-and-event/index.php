<?php
/* @var $this yii\web\View */

$this->title = 'News And Events';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="inner-testimonials">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h4 class="salsa-lesson-main">
                    <?php foreach ($news as $key => $content) {
                        if($key == 0) {
                            continue;
                        }
                        ?>
                        <h4><?= $content->title; ?></h4>
                        <div class="video-blk">
                            <?= $content->content; ?>
                        </div>
                    <?php } ?>
                    <!--salsa-lesson-main-->
                    <div class="clearfix">
                    </div>
            </div>
            <div class="col-md-4 pull-right">
                <div class="video-right">
                    <?php foreach ($news as $key => $content) {
                        if($key > 0) {
                            break;
                        }
                        ?>
                        <h4><?= $content->title; ?></h4>
                        <div class="video-blk1">
                            <?= $content->content; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div><!--row-->
    </div><!--container-->
</section><!--S-rental-->

