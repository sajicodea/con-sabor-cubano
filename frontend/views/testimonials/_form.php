<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin(['id' => 'testimonial-form', 'options' => ['class' => 'reg-fm']]); ?>

<?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => 'Enter Your Name']) ?>

    <?= $form->field($model, 'email')->textInput([ 'placeholder' => 'Enter Email Address']) ?>

    <?= $form->field($model, 'description')->textArea(['rows' => 6, 'placeholder' => 'Message']) ?>

<!--    <img src="img/captcha.png" alt=""/>-->
    <?= Html::submitButton('Submit', ['class' => 'btn btn-default', 'name' => 'contact-button']) ?>
<?php ActiveForm::end(); ?>