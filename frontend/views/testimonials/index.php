<?php
/* @var $this yii\web\View */

$this->title = 'Testimonials';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="inner-testimonials">
    <div class="container">
        <div class="row">
            <div class="input-success">
                <?php if(Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-7">
                <h3>Testimonials</h3>
                <?php foreach($testimonials as $testimonial) {?>
                    <div class="testimonials-inner">
                        <span class="pull-left">"</span>

                        <p>
                            <?= $testimonial->description; ?>
                        </p>
                        <span class="pull-right">"</span>
                    </div><!--testimonials-inner-->
                <?php }?>

            </div><!--col-md-7-->
            <div class="col-md-4 pull-right">
                <div class="submit-testimonials">
                    <h5>Submit Your Testimonial</h5>
                    <?= $this->render('_form', [
                        'model' => $model
                    ])?>
                </div><!--submit-testimonials-->
            </div><!--col-md-7-->
        </div><!--row-->
    </div><!--container-->
</section>
