<?php
/* @var $this yii\web\View */

$this->title = 'SALSA LESSONS';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="inner-testimonials">
    <div class="container">
        <div class="row">
            <h3>SALSA LESSONS</h3>


            <div class="col-sm-12">
                <label class="purchase-heading ">Purchase Training Sessions</label>
                <a target="_blank" href="https://clients.mindbodyonline.com/classic/home?studioid=37534">
                    <button type="submit" class="btn btn-default pay-btn">Purchase</button>
                </a>
            </div>
            <br>
            <br><br>
            <br>

            <div class="col-md-7">
                <div class="input-success">
                    <?php if(Yii::$app->session->hasFlash('success')): ?>
                        <div class="alert alert-success" role="alert">
                            <?= Yii::$app->session->getFlash('success') ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="salsa-lesson-main">
                    <div class=" salsa-lesson">

                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="https://www.youtube.com/embed/lMAk3HgjjvE?wmode=opaque&amp;autohide=1&amp;autoplay=0&amp;color=red&amp;controls=1&amp;loop=0&amp;rel=0&amp;showinfo=0&amp;theme=dark"></iframe>
                        </div>
                    </div><!--salsa-lesson-->
                    <p>
                        Salsa is normally a partner dance, although there are recognized solo forms,
                        line dancing (suelta), and Rueda de Casino where groups of couples exchange
                        partners in a circle. Salsa can be improvised or performed with a set routine.
                        Through the music and dance fusions that are the roots of Salsa.
                        Beginners Salsa Class Description:This course provides an introduction to all of the
                        fundamentals of Salsa. By the end of the course students should be confident enough to get onto
                        the dance floor. This course will lay the foundations by going step-by-step through all the
                        essential moves and breaking them down so they are accessible to everyone.
                    </p>


                    <h4>Every Class will include:</h4>

                    <p>
                        Warm up / Footwork / Dancing in couple / Combinations
                        It’s a fun and a great way to spend time with your significant other and get and closer.
                        Our classes are open to different fitness levels, from those who are new to group fitness
                        programs and/or exercising to those who are active in group fitness programs or another type of
                        exercise program. *It is recommended that individuals who are new to exercise consult with their
                        physician before participating in group fitness classes. Individuals who have been cleared by a
                        physician but have medical conditions such as high blood pressure, arthritis, diabetes, asthma,
                        pregnancy, etc. should lets us know to be aware of their condition in case the instructor has
                        any advice for modifications during the class.
                    </p>
                    <h6>All participants must fill out waiver before participating.</h6>
                    <div class="class-desc-title"><a target="_blank" href="http://96bda424cfcc34d9dd1a-0a7f10f87519dba22d2dbc6233a731e5.r41.cf2.rackcdn.com/CONSABORCUBANO/CSC_consent_form.pdf">Click here to view/print the form.</a></div>

                    <div class="dress_code">
                        <h4>Dress Code</h4>
                        <h6>Please dress casual, wear dancing shoes, soft sole / NO black rubber shoes.</h6>
                    </div><!--dress_code-->

                    <div class="partner">
                        <h4>Don't have a partner? Fill out this form and we will find one for you.</h4>

                        <div class="partner-form">
                            <?= $this->render('_form', [
                                'model' => $model
                            ])?>

                        </div><!--partner-form-->
                    </div><!--partner-->
                </div><!--salsa-lesson-main-->
                <div class="clearfix"></div>
            </div><!--col-md-7-->
            <div class="col-md-4 pull-right">
                <div class="submit-testimonials">
                    <!--                        		<h5>Submit Your Testimonial</h5>
                    -->
                    <div class="lessons">
                        <?= \frontend\components\BlockWidget::widget(['block_keys' => [
                            'group_salsa_lesson'
                        ]]);?>

                    </div><!--lessons-->
                </div><!--submit-testimonials-->


            </div><!--col-md-7-->
        </div><!--row-->
    </div><!--container-->
</section>