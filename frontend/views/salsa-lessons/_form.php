<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin(['id' => 'salsa-request-form', 'options' => ['class' => 'reg-fm']]); ?>

<div class="form-group">

    <?= $form->field($model, 'first_name')->textInput(['placeholder' => 'First Name'])->label(false); ?>

</div>

<div class="form-group">

    <?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Last Name'])->label(false); ?>

</div>
<div class="form-group">

    <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email Address'])->label(false); ?>

</div>
<?php /*echo $form->field($model, 'amount')->inline()->radioList($amountList,[
    'item' => function($index, $label, $name, $checked, $value) {

        $return = '<input class="checkbox" type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
        $return.= '<label>'. $value.'$</label>';

        return $return;
    }
])->label(false);*/ ?>
<?= $form->field($model, 'schedule',
    ['template' => '<p>{labelTitle}</p><div class="checkbox">{input}{error}</div>'])->checkboxList(array(
    'Yes' => 'Yes',
    'No' => 'No'
), [
    'item' => function ($index, $label, $name, $checked, $value) {
        return '<label><input type="checkbox" name="' . $name . '" value="' . $value . '">' . $value . '</label>';
    }
]); ?>

<?= $form->field($model, 'is_new',
    ['template' => '<p>{labelTitle}</p><div class="checkbox">{input}{error}</div>'])->checkboxList(array(
    'Yes' => 'Yes',
    'No' => 'No'
), [
    'item' => function ($index, $label, $name, $checked, $value) {
        return '<label><input type="checkbox" name="' . $name . '" value="' . $value . '">' . $value . '</label>';
    }
]); ?>


<?= $form->field($model, 'experience')->textInput(['placeholder' => $model->getAttributeLabel('experience')])->label(false); ?>

<?= $form->field($model, 'contact')->textarea(['rows' => '5'])->label(true); ?>
<div class="row">
    <div class=" col-md-4">
        <?= $form->field($model, 'gender',
            ['template' => '<p>{labelTitle}</p><div class="checkbox">{input}{error}</div>'])->radioList(array(
            'Male' => 'Male',
            'Female' => 'Female'
        ), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return '<label><input type="radio" name="' . $name . '" value="' . $value . '">' . $value . '</label>';
            }
        ]); ?>
    </div>
    <div class=" col-md-8">
        <?= $form->field($model, 'age_group',
            ['template' => '<p>{labelTitle}</p><div class="checkbox">{input}{error}</div>'])->radioList(array(
            '18-25' => '18-25',
            '25-35' => '25-35',
            '35-45' => '35-45',
            '45-55' => '45-55',
            '55-65' => '55-65'
        ), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return '<label><input type="radio" name="' . $name . '" value="' . $value . '">' . $value . '</label>';
            }
        ]); ?>
    </div>
</div>


<button type="submit" class="btn btn-default">Submit</button>

<?php ActiveForm::end(); ?>
