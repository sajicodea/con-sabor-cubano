<?php
/* @var $this yii\web\View */

$this->title = 'Blog';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>

<section class="inner-testimonials">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h3>Blog</h3>

                <div class="testimonials-inner">
                    <p>Heidi </br>Friday, September 6, 2013</p>
                    <h4>JOANNE'S ZUMBA CLASS WILL BE CANCELED (TEMP)</h4>
                    <p>
                        WE WANT TO INFORM EVERYONE, JOANNE'S ZUMBA CLASS WILL BE CANCELED, JUST FOR THIS COMING WEDNESDAY AT 7:30PM. HER CLASS WILL RESUME BACK NEXT WEDNESDAY.
                    </p>
                </div><!--testimonials-inner-->

            </div><!--col-md-7-->

        </div><!--row-->
    </div><!--container-->
</section>
