<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\assets\UserAsset;

AppAsset::register($this);
UserAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('//layouts/header'); ?>
<?php
    $controller = Yii::$app->controller;
    $isHome = (($controller->id === 'site') && ($controller->action->id === 'index')) ? true : false;
    if($isHome) {
        echo $this->render('//layouts/slider');
    } else {
        echo $this->render('//layouts/banner');
    }
?>
<?= $content ?>
<?= $this->render('//layouts/footer'); ?>
<?= $this->render('//layouts/popup'); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
