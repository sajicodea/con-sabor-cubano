<?php
use yii\helpers\Url;
use kartik\spinner\Spinner;
?>
<header>

    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="logo-top">
                    <a class="navbar-brand" href="<?= Yii::$app->homeUrl?>"><img src="<?= $this->theme->baseUrl; ?>/img/logo.png" width="100%" alt=""/></a>
                </div><!--logo-->
            </div>
            <?php if(!Yii::$app->user->isGuest) {
                $name = Yii::$app->user->identity->username;
            } else {
                $name = '';
            }
            $pageId = '';
            if(isset($_GET['id'])) {
                $pageId = $_GET['id'];
            }
            ?>
            <div id="navbar" class="navbar-collapse collapse">
                <?= \yii\widgets\Menu::widget([
                    'activateItems' => true,
                    'activateParents' => true,
                    'activeCssClass' => 'active-item',
                    'items' => [
                        ['label' => 'HOME', 'url' => ['/'],'options'=> ['class' => 'dropdown'], 'items' => [
                            ['label' => 'Studio Rental', 'url' => ['studio-rental/index']],
                            ['label' => 'Videos', 'url' => ['/videos']],
                        ]],
                        ['label' => 'ABOUT US', 'url' => 'javascript:void(0)','options'=> ['class' => 'dropdown'], 'items' => [
                            ['label' => 'Culture & History', 'url' => ['/culture-and-history'],'active' =>$pageId == 'culture-and-history'],
                            ['label' => 'Instructors', 'url' => ['/instructors'],'active' =>$pageId == 'instructors'],
                            ['label' => 'Testimonials', 'url' => ['testimonials/index']],
                            ['label' => 'Location', 'url' => ['site/location']],
                            ['label' => 'Blog', 'url' => ['/blog/ ']],
                            ['label' => 'Contact', 'url' => ['site/contact']],
                        ]],
                        ['label' => 'CLASSES', 'url' => ['/classes'],'options'=> ['class' => 'dropdown'],'active' =>$pageId == 'classes', 'items' => [
                            ['label' => 'About Membership', 'url' => ['/membership'],'active' =>$pageId == 'membership'],
                            ['label' => 'Class Schedule', 'url' => ['class/schedule']],
                            ['label' => 'Class Descriptions', 'url' => ['class/class-description']],
                            ['label' => 'Signup for a Class', 'url' => ['class/schedule']],
                            ['label' => 'Policies & Regulations', 'url' => ['/class-rules-regulations'],'active' =>$pageId == 'class-rules-regulations'],
                        ]],
                        ['label' => 'FREE CLASS', 'url' => ['/special-promotion'],'options'=> ['class' => ''], 'active' =>$pageId == 'special-promotion'],
                        ['label' => 'PERSONAL TRAINING', 'url' => ['/personal-training'],'options'=> ['class' => 'dropdown'],'active' =>$pageId == 'personal-training', 'items' => [
                            ['label' => 'Nutrition & Weight Loss', 'url' => ['/nutrition-and-weight-loss'],'active' =>$pageId == 'nutrition-and-weight-loss'],
                        ]],
                        ['label' => 'SALSA LESSONS', 'url' => ['salsa-lessons/index'],'options'=> ['class' => '']],
                        ['label' => 'PURCHASE', 'url' => 'http://clients.mindbodyonline.com/ws.asp?studioid=37534&stype=41','options'=> ['class' => '']],
                        ['label' => 'NEWS/EVENTS', 'url' => ['news-and-event/index'],'options'=> ['class' => '']],
                        ['label' => 'GALLERY', 'url' => ['gallery/index'],'options'=> ['class' => '']],
                        [
                            'label' => 'Register',
                            'url' => Url::to(['site/register']),
                            'visible' => Yii::$app->user->isGuest,
                            //'template' => '<a class="showModalButton" id="client-login" title="Login" value="{url}" href="javascript:void(0)">{label}</a>'
                        ],
                        ['label' => 'Buy', 'url' => 'javascript:void(0)','options'=> ['class' => 'dropdown'], 'items' => [
                            ['label' => 'Buy Memberships', 'url' => 'https://clients.mindbodyonline.com/asp/main_shop.asp'],
                            ['label' => 'Gift cards', 'url' => 'https://clients.mindbodyonline.com/asp/main_shop.asp'],
                        ]],
                        ['label' => 'Logout', 'url' => Url::to(['site/logout']), 'visible' => !Yii::$app->user->isGuest],
                    ],
                    'linkTemplate' => '<a class="dropdown-toggle" href="{url}">{label}</a>',
                    'options' => ['class' => ''],
                    'submenuTemplate' => "\n<ul class='dropdown-menu'>\n{items}\n</ul>\n"
                ]); ?>

            </div><!--/.navbar-collapse -->
        </div>
    </nav>


</header>
<?= Spinner::widget([
    'preset' => Spinner::LARGE,
    'color' => 'white',
    'align' => 'top',
    'hidden' =>true
])?>