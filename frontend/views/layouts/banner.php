<section class="inner-banner">
    <div class="inner-banner-content">
        <h2>Dance & <span>Fitness</span></h2>
        <h6>Dance, be healthy , Be Fit, <span>feel the passion</span></h6>
    </div><!--inner-banner-content-->
<?php if(isset(Yii::$app->controller->pageSlug)){ ?>
    <?php if(Yii::$app->controller->pageSlug=="classes"){ ?>
    <img src="<?= $this->theme->baseUrl; ?>/img/class-banner02.jpg" class="img-responsive" alt=""/>
    <?php }else{ ?>
    <img src="<?= $this->theme->baseUrl; ?>/img/class-banner.jpg" class="img-responsive" alt=""/>
    <?php } ?>
    
<?php } else {?>
    <img src="<?= $this->theme->baseUrl; ?>/img/class-banner.jpg" class="img-responsive" alt=""/>
<?php } ?>
    <div class="inner-navigation">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 brd-lst">
                    <?= \yii\widgets\Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'itemTemplate'=>'{link}<span>></span>',
                        'homeLink' => [
                            'label' => Yii::t('yii', 'Home'),
                            'url' => Yii::$app->homeUrl
                        ],
                        'encodeLabels' => false,
                        'tag' => false,
                        'options' => ['class' => 'brd-lst']
                    ]) ?>
                </div><!--col-sm-12-->
            </div><!--row-->
        </div><!--container-->
    </div><!--inner-navigation-->
</section><!--inner-banner-->