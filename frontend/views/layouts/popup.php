<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'closeButton' => ['label' => 'x','tag'=>'Button'],
    'size' => 'modal-md wd-mod',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    //'clientOptions' => ['backdrop' => 'static','closeButton' => true]
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
?>