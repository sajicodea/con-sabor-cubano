<section class="news-letter">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Newsletter</h3><h6>Sign Up Here To Receive Our Email Newsletter!</h6>
            </div><!--col-md-6-->
            <div class="col-md-6">
                <?= \frontend\components\SubscribeWidget::widget(); ?>

            </div><!--col-md-6-->
        </div><!--row-->
    </div><!--container-->
</section><!--news-letter-->
<footer class="footer">
    <div class="container">
        <div class="row">

            <div class="col-md-2">
                <h6>Main links</h6>
                <?=\yii\widgets\Menu::widget([
                    'items' => [

                        ['label' => 'Home', 'url' => ['/'],'options'=> ['class' => '']],
                        ['label' => 'About Us', 'url' => ['/about-us'],'options'=> ['class' => '']],
                        ['label' => 'Classes', 'url' => ['/class'],'options'=> ['class' => '']],
                        ['label' => 'Personal Training', 'url' => ['/personal-training'],'options'=> ['class' => '']],
                        ['label' => 'Salsa Lessons', 'url' => ['/salsa-lessons'],'options'=> ['class' => '']],
                        ['label' => 'News/Events', 'url' => ['/news-events'],'options'=> ['class' => '']],
                        ['label' => 'Blog', 'url' => ['/blog'],'options'=> ['class' => '']],
                        ['label' => 'Contact Us', 'url' => ['/contact-us'],'options'=> ['class' => '']],
                    ],
                ]);
                ?>
            </div><!--col-md-3-->
            <div class="col-md-4">
                <h6>LAtest Tweets</h6>
                <p><span>Mar 7, 2012 1:16 pm</span>
                    We have 3 new Zumba Instructors coming on aboard with Michel and I. Please check the Bios on our Website.
                    <a href="https://twitter.com/consaborcubano/status/177502964473274368" target="_blank"> Read More</a></p>

                <p><span>June 15, 2012 7:18 pm</span>
                    We are inviting everyone who love to do Zumba Fitness and support us,
                    for a great cause.<br>
                    <a href="https://twitter.com/consaborcubano/status/213817828371996672" target="_blank">Read More</a></p>
            </div><!--col-md-3-->
            <div class="col-md-3">
                <h6>Quick links</h6>
                <?=\yii\widgets\Menu::widget([
                    'items' => [

                        ['label' => 'Culture & History', 'url' => ['/culture-and-history'],'options'=> ['class' => '']],
                        ['label' => 'About Memberships', 'url' => ['/membership'],'options'=> ['class' => '']],
                        ['label' => 'Class Schedule', 'url' => ['/class-schedule'],'options'=> ['class' => '']],
                        ['label' => 'Sign Up For A Class', 'url' => ['/class'],'options'=> ['class' => '']],
                        ['label' => 'Purchases', 'url' => 'https://clients.mindbodyonline.com/classic/home?studioid=37534','options'=> ['class' => '']],
                        ['label' => 'Studio Rules & Regulations', 'url' => ['/studio-rules-and-regulations'],'options'=> ['class' => '']],
                        ['label' => 'Membership Agreement', 'url' => ['/membership-agreement'],'options'=> ['class' => '']],
                        ['label' => 'Class Rules & Regulations', 'url' => ['/class-rules-regulations'],'options'=> ['class' => '']],
                    ],
                ]);
                ?>
            </div><!--col-md-3-->
            <div class="col-md-3">
                <h6>Address</h6>
                <address>
                    3550 San Pablo Dam Road Suite F <br>
                    El Sobrante, CA<br>
                    Tel: 510-222-6300
                </address>
                <h6>Social Networks</h6>
                <a href="https://www.facebook.com/Con-sabor-cubano-343781565664868/timeline?ref=page_internal"
                   target="_blank">
                    <img src="<?= $this->theme->baseUrl; ?>/img/fb.png" class="social-ico" width="27" height="27" alt="Facebook"/></a> &nbsp;
                <a href="https://twitter.com/consaborcubano/status/177502964473274368" target="_blank">
                    <img src="<?= $this->theme->baseUrl; ?>/img/twet.png" class="social-ico" width="27" height="27" alt="Twitter"/></a>&nbsp;
                <a href="http://www.yelp.com/biz/con-sabor-cubano-dance-and-fitness-el-sobrante#query:Con%20Sabor%20Cubano"
                   target="_blank">
                    <img src="<?= $this->theme->baseUrl; ?>/img/yelp.png" class="social-ico" width="27" height="27" alt="Yelp"/></a>&nbsp;
                <a href="http://www.yellowpages.com/el-sobrante-ca/mip/con-sabor-cubano-dance-fitness-478332461?lid=478332461"
                   target="_blank">
                    <img src="<?= $this->theme->baseUrl; ?>/img/yellow-p.png" width="28" class="social-ico" height="28" alt="Yellow Pages"/></a>
                <a href="https://www.youtube.com/user/HMzumbafitness?feature=guide" target="_blank">
                    <img src="<?= $this->theme->baseUrl; ?>/img/you-tube.png" width="28" class="social-ico" height="28" alt="You ube"/></a>
            </div><!--col-md-3-->
            <div class="col-sm-12 sep"></div>
            <div class="col-sm-6 ft">
                <a href="#">Terms and Conditions</a> | <a href="#">Privacy Policy</a> © 2016 All Rights Reserved,
            </div><!--col-sm-6-->
            <div class="col-sm-3 pull-right ft">
                Designed and Developed by : <a href="http://codeaweb.com/" target="_blank">Codea</a>
            </div><!--col-sm-6-->

        </div><!--row-->
    </div><!--container-->
</footer><!--footer-->