<div class="slider">
    <div id="owl-slide1" class="owl-carousel owl-theme">

        <div class="item">
            <div class="container">
                <div class="row">
                    <div class="slide-content">
                        <h2>Dance & <span>Fitness</span></h2>
                        <h6>Dance, be healthy , Be Fit,  <span>feel the passion</span></h6>
                    </div><!--slide-content-->
                </div><!--row-->
            </div><!--container-->
            <img src="<?= $this->theme->baseUrl; ?>/img/top-slide2.jpg" class="img-responsive" alt=""/>
        </div><!--item-->

        <div class="item">
            <div class="container">
                <div class="row">
                    <div class="slide-content">
                        <h2 class="bannr-h2">TRY THE 30-DAY CIZE DOWN!</span></h2>
                        <p class="bannr-titl">
                            Forget everything you dread about workouts. Because starting today, exercize isn't something you have to do, it's something you'll want to do.
                            Beachbody® is about to show you how to dance your way to fit in just 30 days! Let loose as you learn the hottest professionally choreographed dance workouts. You'll be so focused on mastering the moves and having a total blast, you'll forget you're actually working out—and losing weight!
                            This is your chance to "dance like no one's watching"—in the comfort of your own home.
                        </p>
                    </div><!--slide-content-->
                </div><!--row-->
            </div><!--container-->
            <img src="<?= $this->theme->baseUrl; ?>/img/top-slide4.jpg" class="img-responsive" alt=""/>
        </div><!--item-->

        <div class="item">
            <div class="container">
                <div class="row">
                    <div class="slide-content">
                        <h2>Dance & <span>Fitness</span></h2>
                        <h6>Dance, be healthy , Be Fit,  <span>feel the passion</span></h6>
                    </div><!--slide-content-->
                </div><!--row-->
            </div><!--container-->
            <img src="<?= $this->theme->baseUrl; ?>/img/top-slide1.jpg" class="img-responsive" alt=""/>
        </div><!--item-->

    </div><!--owl-slide1-->
    <div class="arrow-slider"><a href="#"><img src="<?= $this->theme->baseUrl; ?>/img/button.png" alt=""/></a></div><!--arrow-slider-->
</div><!--slider-->