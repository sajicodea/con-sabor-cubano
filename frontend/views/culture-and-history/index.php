<?php
/* @var $this yii\web\View */

$this->title = 'Culture & History';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>

    <section class="welcome missionstatement">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="welcome-img">
                        <div class="border-1"></div><!--border-1-->
                        <div class="register-btn"><a href="<?= \yii\helpers\Url::to(['/register'])?>">Register Now</a></div><!--register-btn-->
                        <img src="<?= $this->theme->baseUrl; ?>/img/welcome-image.jpg" class="img-responsive" alt=""/>
                    </div><!--welcome-img-->
                </div><!--col-sm-4-->
                <div class="col-sm-8 welcome-right">
                    <h1>Our Mission Statement</h1>
                    <p>
                        Con-Sabor-Cubano, Dance & Fitness is a health & fitness venue that helps individuals attain one of the greatest gifts of all; a good health, Personal gains, such as improved self-esteem, and self-motivation. We offer a place where people can learn to dance, increase their fitness capacity.  Meet new people, have fun and feel comfortable. We offer a varied dance fitness program with price options for all levels of interest, with greater emphasis on group classes and small package sessions to reach dance skill objectives. Our instructors have access to continual training with some of the area's top professional coaches. This provides our students with up-to-date steps and technique and access to the latest dance trends.
                    </p>
                </div><!--col-sm-8 welcome-right-->
            </div><!--row-->
        </div><!--container-->
    </section>
<section class="inner-testimonials">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
               <?php foreach ($data as $content) {?>
                   <div class="dv-h3"><?= $content->title;?></div>
                    <div>
                        <?= $content->description;?>
                    </div>
                <?php }?>
            </div><!--col-sm-12-->


        </div><!--row-->
    </div><!--container-->
</section><!--S-rental-->
