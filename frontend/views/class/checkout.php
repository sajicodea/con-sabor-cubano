<?php
/**
 * Created by PhpStorm.
 * User: jobin
 * Date: 5/8/2016
 * Time: 11:39 PM
 */
?>

<?php
/* @var $this yii\web\View */

$this->title = 'Purchase';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<?php
$url = \yii\helpers\Url::to(['/product-buy']);
$format = <<< SCRIPT
function product_checkout(id)
{
    var ids = $("#product-list-"+id+" input[type='radio']:checked");
    if(ids.length <=0) {
        alert('Please choose a product!');
    } else {
        if(ids.val()) {
            window.location.href="$url"+"/"+ids.val();
        }
    }
}
SCRIPT;
$this->registerJs($format, \yii\web\View::POS_HEAD);
?>
<section class="S-rental">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="dv-h3"><?= 'Welcome, ' . $user['FirstName'] . '!'; ?></div>
                <div class="wht-bx">
                    <?php if ($confirm) { ?>
                        <h4>You are signing up for:</h4>
                        <?php
                        $startDate = date('D m/d/Y', strtotime($class['StartDateTime']));
                        $startDateTime = date('h:i a', strtotime($class['StartDateTime']));
                        $endDateTime = date('h:i a', strtotime($class['EndDateTime']));
                        ?>
                        <div>
                            <?= $class['ClassDescription']['Name']; ?><br>
                            on <?= $startDate ?> at <?= $startDateTime ?> - <?= $endDateTime ?>
                        </div>

                        <a class="btn btn-default" href="<?= \yii\helpers\Url::to(['/class-schedule']) ?>">Cancel</a>
                        <a class="btn  btn-warning" href="<?= \yii\helpers\Url::to(['/checkout/' . $id]) ?>">Confirm</a>
                    <?php } elseif (isset($response['Clients']['Client']['Messages'])) { ?>
                    <?php
                    $startDate = date('D m/d/Y', strtotime($class['StartDateTime']));
                    $startDateTime = date('h:i a', strtotime($class['StartDateTime']));
                    $endDateTime = date('h:i a', strtotime($class['EndDateTime']));
                    ?>
                    <div class="row">
                        <div class=" col-sm-7 alert alert-danger">You do not have the required account credit to sign up for this
                            item.
                        </div>
                    </div>
                </div>
                Please choose an item to purchase: <br>
                In order to sign up for <?= $class['ClassDescription']['Name']; ?> on <?= $startDate ?> at <?= $startDateTime ?> - <?= $endDateTime ?>, you must purchase one of the
                following items:
                <div class="col-sm-12 item-blk">
                    <h3>Please choose an item to purchase:</h3>
                    <?php foreach ($programs as $id => $program) {
                        $services = $api->getServices($id);
                        if(count($services) > 0) {
                            ?>
                            <div class="item-row">
                                <h4><?= $program; ?></h4>
                                <ul id="product-list-<?=$id?>">
                                    <?php
                                    foreach ($services as $service) {
                                        if(isset($service['ProductID'])) {

                                            ?>
                                            <li>
                                                <input type="radio" name="product<?=$id?>" value="<?= $service['ProductID'] ?>">
                                                $<?= $service['Price'] ?> - <?= $service['Name'] ?>
                                            </li>
                                        <?php }
                                    } if(isset($services['ProductID'])) { ?>
                                        <li>
                                            <input type="radio" name="product" value="<?= $services['ProductID'] ?>">
                                            $<?= $services['Price'] ?> - <?= $services['Name'] ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <a href="javascript:product_checkout(<?=$id?>)" class="btn btn-default nxt-tbn">NEXT</a>
                            </div>
                            <!--REQ-form-->
                        <?php } }?>
                </div><!--col-sm-10-->
                <?php } elseif(isset($success)) {
                    echo '<p> The product has been successfully purchased</p>';
                } ?>
            </div>
        </div><!--col-sm-12-->
    </div><!--row-->
</section><!--S-rental-->
