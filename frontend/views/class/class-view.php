<?php
/* @var $this yii\web\View */

$this->title = 'Class View';
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['/class']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];

$staffs = array();
?>
<section class="S-rental">
    <div class="container">
        <div class="row">
            <?php if (Yii::$app->user->isGuest) { ?>
                <div class="col-sm-12">
                    <?= \frontend\components\LoginWidget::widget(); ?>
                </div>
            <?php } ?>
            <div class="col-md-12 class-schedule-blk">
                <div class="col-sm-7 row ">
                    <?php if (isset($description['ImageURL']) > 0) { ?>
                        <h3><?= $description['Name']; ?></h3>
                        <div class="testimonials-inner">
                            <div class="class-img">
                                <img src="<?= $description['ImageURL'] ?>">
                            </div>
                            <div class="wht-txt"><?= $description['Description']; ?></div>
                        </div>
                    <?php } ?>
                    <?php if (count($classes) > 0) { ?>
                        <h3><br><br>Upcoming classes:</h3>
                    <?php } ?>
                    <ul class="class-dtl class-dtl-list ">
                        <?php
                        foreach ($classes as $classDate => $classes) {
                            ?>
                            <tbody>
                            <?php foreach ($classes as $class) {
                                $sDate = date('H:i A', strtotime($class['StartDateTime']));
                                $classDate = date('M d', strtotime($class['StartDateTime']));
                                $sLoc = $class['Location']['Name'];
                                $sTG = $class['ClassDescription']['Program']['ID'];
                                $studioid = $class['Location']['SiteID'];
                                $sclassid = $class['ClassScheduleID'];
                                $sType = -7;
                                $ID = $class['ID'];
                                $linkURL = "https://clients.mindbodyonline.com/ws.asp?sDate={$sDate}&sLoc={$sLoc}&sTG={$sTG}&sType={$sType}&sclassid={$sclassid}&studioid={$studioid}";
                                //$linkURL = \yii\helpers\Url::to(['checkout/' . $ID . '?confirm=1']);
                                $className = $class['ClassDescription']['Name'];
                                $startDateTime = date('h:i a', strtotime($class['StartDateTime']));
                                $endDateTime = date('h:i a', strtotime($class['EndDateTime']));
                                $staffName = $class['Staff']['Name'];
                                $staffs[$class['Staff']['ID']] = $class['Staff'];
                                //echo "<a href='{$linkURL}'>{$className}</a> w/ {$staffName} {$startDateTime} - {$endDateTime}<br />";
                                ?>
                                <li class="class-time">
                                    <?= $classDate . ' ' . $startDateTime . ' - ' . $endDateTime . ' with ' . $staffName; ?></td>

                                    <a href="<?= $linkURL; ?>">
                                        <button type="button" class="class-btn">Sign Up</button>
                                    </a>
                                </li>
                            <?php } ?>
                            </tbody>
                        <?php } ?>
                    </ul>
                </div>
                <div class="row instruc-list">
                    <div class="col-sm-4 pull-right instructer-bg">
                        <h4>This class is taught by:</h4>
                    </div>
                    <?php
                    foreach ($staffs as $staffID => $staff) {
                        ?>
                        <br><br><br><br><br><br><br><br>

                        <div class="col-sm-4 pull-right instructer-bg">
                            <?php if (isset($staff['ImageURL']) && $staff['ImageURL'] != 'No Image') { ?>
                                <div class="instructor-thumb">
                                    <img src="<?= $staff['ImageURL'] ?>" width="100%">
                                </div>
                            <?php } ?>
                            <div class="col-sm-9 pull-right">
                                <h3>
                                    <a href="<?= \yii\helpers\Url::to(['staff/' . $staffID]) ?>"><br><?= $staff['Name']; ?> </a>
                                </h3>
                            </div>
                        </div>
                        <?php
                    } ?>
                </div>
            </div>
        </div><!--col-sm-12-->


    </div><!--row-->
</section><!--S-rental-->