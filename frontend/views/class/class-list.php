<?php
/* @var $this yii\web\View */

$this->title = 'Classes';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="S-rental">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="dv-h3"><?= $this->title;?></div>
                <div>
                    <ul>
                        <?php foreach($classes as $key => $class) {?>
                            <li><a href="<?= \yii\helpers\Url::to(['class/'.$key])?>"><?= $class?></a></li>
                        <?php }?>
                    </ul>
                </div>
            </div><!--col-sm-12-->


        </div><!--row-->
    </div><!--container-->
</section><!--S-rental-->