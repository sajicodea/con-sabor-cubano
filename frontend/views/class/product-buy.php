<?php
use yii\helpers\Url;
use yii\helpers\Html;
use \yii\bootstrap\ActiveForm;

/**
 * Created by PhpStorm.
 * User: jobin
 * Date: 5/8/2016
 * Time: 11:39 PM
 */
?>

<?php
/* @var $this yii\web\View */

$this->title = 'Purchase';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="S-rental">
    <div class="container">
        <div class="input-success">
            <?php if(Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="input-error">
            <?php if(Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>
        </div>
        <?php if(!Yii::$app->session->hasFlash('success')): ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="dv-h3">You are purchasing:</div>
                <div class="wht-bx">
                    <?= $service['Name'] ?> <br>
                    $<?= number_format($service['Price'], 2, '.', false) ?> <br>

                    Total: $<?= number_format($service['Price'], 2, '.', false) ?>
                </div>
            </div><!--col-sm-12-->
            <div class="row reg-fm">
                <div class="col-lg-6">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-client-form',
                        //'action' => Url::to(['site/client-login']),
                        'enableAjaxValidation' => false,
                        //'validationUrl' => Url::to(['site/ajax-client-login']),
                    ]); ?>
                    <?= $form->field($model, 'name')->textInput() ?>
                    <?= $form->field($model, 'address')->textInput() ?>
                    <?= $form->field($model, 'city')->textInput() ?>
                    <?= $form->field($model, 'state')->textInput() ?>
                    <?= $form->field($model, 'country')->dropDownList(\yii\helpers\ArrayHelper::map($country,
                        'iso_code_2',
                        'name')) ?>
                    <?= $form->field($model, 'zip')->textInput() ?>
                    <?= $form->field($model, 'card_no')->textInput(['value' => '']) ?>
                    <?php
                    $monthList = range(01, 12);
                    $months = array();
                    foreach ($monthList as $month) {
                        $months[$month] = date("F", mktime(0, 0, 0, $month, 10));
                    }
                    $years = date('Y');
                    $years = range($years, $years + 8);
                    $years = array_combine($years, $years);
                    ?>

                    <label>Expiration Date:</label>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'expiry_year')->dropDownList($years)->label(false) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'expiry_month')->dropDownList($months)->label(false) ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'save_info',
                        ['template' => '<p>{labelTitle}</p><div class="checkbox">{input}{error}</div>'])->checkbox(/*array(
                    'Yes' => 'Yes',
                    'No' => 'No'
                ), [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return '<label><input type="checkbox" name="' . $name . '" value="' . $value . '">' . $value . '</label>';
                    }
                ]*/); ?>
                    <div class="form-group">
                        <?= Html::submitButton('Process', ['class' => 'btn btn-primary', 'name' => 'process-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div><!--row-->
        <?php endif; ?>
    </div><!--container-->
</section><!--S-rental-->
