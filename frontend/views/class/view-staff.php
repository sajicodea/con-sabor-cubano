<?php
/* @var $this yii\web\View */

$this->title = 'Instructor';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="p-training">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <div class="col-sm-12">
                            <?= \frontend\components\LoginWidget::widget(); ?>
                        </div>
                    <?php } ?>

                    <div class="row">
                        <?php if (isset($staff['ImageURL']) && $staff['ImageURL'] != 'No Image') { ?>
                            <div class="col-sm-3">
                                <img src="<?= $staff['ImageURL']; ?>" class="instructor-img" width="100%" alt=""/>
                            </div>
                        <?php } ?>
                        <div class="col-sm-9">
                            <h4><?= $staff['Name']; ?></h4>

                            <div class="wht-txt">
                                <?= $staff['Bio']; ?>
                            </div>
                            <?php foreach($classes as $class) {?>
                                <h6><a href="<?= \yii\helpers\Url::to(['class/' . $class['ID']]) ?>"><?= $class['Name']?>  </a></h6>
                                <div class="wht-txt"><?= $class['Description']?></div>
                            <?php }?>
                        </div>

                    </div>
                    <div class="separator"></div>

                </div>
            </div><!--col-sm-12-->
        </div><!--row-->
    </div><!--container-->
</section>
