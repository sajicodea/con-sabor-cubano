<?php
/* @var $this yii\web\View */
?>
<section class="S-rental">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Classes</h3>

                <div class="clas-blk1">
                    <div class="col-lg-7">
                        <h4>3 Day Membership</h4>

                        <p>This is a Limited Access Membership that entitles a new member to the facility at limited
                            times through out the week. You choose 3 days either AM Monday-Sunday or PM classes Monday
                            to Friday classes. Classes and times are subject to change without notice.
                        </p>
                        <ul>
                            <li>$65 Unlimited Membership</li>
                            <li>$55 Limited Membership</li>
                            <li>$50 Unlimited Mornings</li>
                            <li>$45 3-Day Membership</li>
                            <li>$12 Per Class Paid online ($15 Paid in studio)</li>
                            <li>Online Pre-Registration is require</li>
                        </ul>
                    </div>
                    <div class="col-lg-5">
                        <img src="img/class-img1.jpg" class="img-responsive" alt=""/></div>
                </div>

                <div class="clearfix"></div>

            </div><!--col-sm-12-->

            <div class="col-lg-12">
                <div class="clas-blk2">
                    <div class="col-lg-6">
                        <h4>Unlimited Membership </h4>
                        <ul>
                            <li>$65 Month to Month (Auto-pay)</li>
                            <li>Pay 1st and last month</li>
                        </ul>
                        <h4>Limited Membership</h4>
                        <ul>
                            <li>10 classes a Month</li>
                            <li>$55 Month to Month (Auto-pay)</li>
                            <li>Pay 1st and last month</li>
                        </ul>
                        <h4>Unlimited Morning Membership<br>
                            (Access to all AM classes, no evenings classes)</h4>
                        <ul>
                            <li>$50 Month to Month (Auto-pay)</li>
                            <li>Pay 1st and last month</li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <h4>3 Day-Membership (AM or PM)</h4>
                        <ul>
                            <li>$45 Month-to-Month (Auto-pay)</li>
                            <li>Pay 1st and Last Month</li>
                            <li>Interest in taking a few classes throughout the month, with no memberships</li>
                            <li>We have two PUNCH CARDS. Punch cards are pre-paid cards.</li>
                            <li>$45 Punch Card (5 classes) value one class $12-$15 Good for 1 Month</li>
                            <li>$65 Punch Card (10 classes) value one class $12-$15 Good for 2 months</li>
                        </ul>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="clas-blk3">
                    <h3>COMING SOON</h3>
                    <h4>CORPORATE MEMBERSHIP , FAMILY MEMBERSHIP, SENIORS DISCOUNTED RATES AVAILABLE
                        NO CONTRACTS, NO FEES, DISCONTINUED RATES</h4>
                </div>
            </div>

            <div class="col-sm-12 proms-blk">
                <h2>ALL SPECIAL PROMOTIONS ARE DONE AT THE STUDIO</h2>

                <h3>If you are interested in getting more information about our monthly specials and promotions<br>
                    Call us at 510-222-6300 to schedule an appointment or send us an email to fitnessinfo@att.net. We
                    look forward to meeting you.</h3>
                <!--REQ-form-->
                <div class="prm01">
                    <div class="rows">
                        <h4>Unlimited Membership</h4>
                        <h5>$65 REGISTRATION<br>
                            $65 PER MONTH
                        </h5>

                        <p>
                            Same privileges as Limited Membership with unlimited participation in all regularly
                            scheduled group exercise and dance classes on the regularly published studio schedules.
                            Membership will renew automatically each month term on a month-to-month basis unless I give
                            30 day written notice to cancel and return my membership card. For a replacement on a lost
                            tag/card, there's a fee of $10.
                        </p>
                    </div>
                    <div class="rows">
                        <h4>Unlimited Morning Memberships</h4>
                        <h5>
                            $50 REGISTRATION<br>
                            $50 per month<br>
                            (expires in 1 month)
                        </h5>

                        <p>
                            Same Privileges as Limited Membership with Unlimited participation in all regularly
                            scheduled morning group exercise and dance classes on the regularly published studio
                            schedules. Membership will renew automatically at end of month term on a month-to-month
                            basis unless a 30 day written notice to cancel and return membership scan card is given.my
                            membership card. For a replacement on a lost tag/card, there's a fee of $10.
                        </p>
                    </div>
                    <div class="rows adj">
                        <h4>PAYMENT OPTIONS</h4>
                        <ul>
                            <li>CASH</li>
                            <li>CREDIT / DEBIT CARD (We Accept MC, Visa American Express, Discover)</li>
                            <li>EFT AUTO PAY</li>
                        </ul>
                    </div>
                </div>

            </div><!--col-sm-10-->
        </div><!--row-->
    </div><!--container-->
</section>
