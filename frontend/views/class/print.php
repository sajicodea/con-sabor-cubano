<style type="text/css">
    .desc {
        margin-left: 20px;
        font-size: 11px;
    }
    .class_item {
        padding: 5px;
    }
    .class_name {
        font-size: 1.6em;
        font-weight: 600;
    }
    .instructor_name {
        font-size: .6em;
        font-style: italic;
    }
    .master_date {
        width: 100%;
        font-size: 1.1em;
        font-weight: 700;
        border-bottom: 1px solid;
        height: 25px;
        padding-top: 5px;
        margin-bottom: 3px;
    }
    .title_date {
        float: left;
        width: 30%;
        padding-left: 25px;
        text-align: left;
    }
    .title_class {
        float: left;
        width: 30%;
        padding-left: 20px;
        text-align: left;
    }
    .title_staff {
        float: left;
        width: 20%;
        text-align: left;
    }
    .mb_listview_date {
        display: inline-block;
        text-align: center;
        width: 100%;

    }
    #forward_week {
        float: right;
    }
    #back_week {
        float: left;
    }
    #le_mb_list_schedule{
        margin: auto;
        padding: 15px;
        width: 95%;
    }

    .crossout {
        text-decoration: line-through;
    }
    .row_date {
        width:30%;
        text-align: left;
        padding-left: 3%;
    }
    .row_class {
        width:30%;
        text-align: left;
        padding-left: 5%;
    }
    .row_staff {
        width:20%;
        text-align: left;
        padding-left: 5%;"
    }
    .sched_resource {
        display: block;
        padding-top: 2px;
    }
    .row_date, .row_class, .row_staff {
        vertical-align: top;
    }
    .row_button {
        vertical-align: top;
        text-align: center;
    }

    .le_metro_silver_button:hover, .le_metro_silver_button a:hover {
        text-decoration: none;
        cursor: hover;
    }
    .schedule_filters {
        float: right;
        width:  100%;
        /*	border: 1px solid #333;*/
    }
    .dropdowns {
        width: 25%;
        /*	border: 1px solid #333;*/
        text-align: left;
    }
    .selects {
        width: 90%;
    }
    #date_filter{
        width: 80px;
    }
    body table{
        font-size: .9em;
        width: 100%;
    }

</style>



<div class="le_mb_wrapper">

    <div id="le_mb_list_schedule">

        <h2>
	<span class="mb_listview_date">
				<span class="schedule_date_range">Schedule for Wed May 18, 2016 - Tue May 24, 2016</span>
	</span>
        </h2>
        <br>


        <div class="master_date mbHeaderBar">
            <div class="title_date ">Wed May 18, 2016</div>
            <div class="title_class">Class</div>
            <div class="title_staff">Instructor</div>
        </div>
        <table>
            <tr>

                <td class="row_date">8:30 am - 9:00 am</td>
                <td class="row_class">
                    T.B.C Express (Total Body Condition)
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">9:00 am - 9:30 am</td>
                <td class="row_class">
                    Core-Blast
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">9:30 am - 10:30 am</td>
                <td class="row_class">
                    Zumba
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:00 pm - 6:30 pm</td>

                <td class="row_class">
                    Core-Blast
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:30 pm - 7:30 pm</td>
                <td class="row_class">
                    P90X Live Workout
                    <span class="sched_resource">STUDIO 2</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">7:00 pm - 8:00 pm</td>
                <td class="row_class">
                    Zumba
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Willa</span>
                    <span class="mb_le_staff_lastname">WJ.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">8:00 pm - 9:00 pm</td>
                <td class="row_class">
                    Yoga- Vinyasa (Level 1-2)
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Dalila</span>
                    <span class="mb_le_staff_lastname">S</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>



        </table>


        <div class="master_date mbHeaderBar">
            <div class="title_date ">Thu May 19, 2016</div>
            <div class="title_class">Class</div>
            <div class="title_staff">Instructor</div>
        </div>
        <table>
            <tr>

                <td class="row_date">8:30 am - 9:30 am</td>
                <td class="row_class">
                    Yoga- Vinyasa (Level 1-2)
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Dalila</span>
                    <span class="mb_le_staff_lastname">S</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">9:30 am - 10:30 am</td>
                <td class="row_class">
                    Zumba
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Willa</span>
                    <span class="mb_le_staff_lastname">WJ.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:30 pm - 7:30 pm</td>
                <td class="row_class">
                    Body Toning
                    <span class="sched_resource">STUDIO 2</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Michel</span>
                    <span class="mb_le_staff_lastname">R</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:30 pm - 7:30 pm</td>
                <td class="row_class">
                    Cardio Kick
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">7:30 pm - 8:30 pm</td>
                <td class="row_class">
                    Dance Fusion
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">JAY</span>
                    <span class="mb_le_staff_lastname">L.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">7:30 pm - 8:30 pm</td>
                <td class="row_class">
                    Cardio Boot Camp
                    <span class="sched_resource">STUDIO 2</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Leyder</span>
                    <span class="mb_le_staff_lastname">C.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>



        </table>


        <div class="master_date mbHeaderBar">
            <div class="title_date ">Fri May 20, 2016</div>
            <div class="title_class">Class</div>
            <div class="title_staff">Instructor</div>
        </div>
        <table>
            <tr>

                <td class="row_date">8:30 am - 9:00 am</td>
                <td class="row_class">
                    T.B.C Express (Total Body Condition)
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Uma</span>
                    <span class="mb_le_staff_lastname">M.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">9:00 am - 9:30 am</td>
                <td class="row_class">
                    Core-Blast
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">9:30 am - 10:30 am</td>
                <td class="row_class">
                    Zumba
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Uma</span>
                    <span class="mb_le_staff_lastname">M.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:30 pm - 7:00 pm</td>
                <td class="row_class">
                    Zumba with Weights
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">7:00 pm - 7:30 pm</td>
                <td class="row_class">
                    Core-Blast
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">7:30 pm - 8:30 pm</td>
                <td class="row_class">
                    Afro-Salsa Cuban Style
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Royland</span>
                    <span class="mb_le_staff_lastname">L</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>



        </table>


        <div class="master_date mbHeaderBar">
            <div class="title_date ">Sat May 21, 2016</div>
            <div class="title_class">Class</div>
            <div class="title_staff">Instructor</div>
        </div>
        <table>
            <tr>

                <td class="row_date"><span class="mb_HolidayNotice">TURBO KICK CERTIFICATION 9AM-5PM</span></td>

                <td class="row_class">

                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname"></span>
                    <span class="mb_le_staff_lastname"></span>
                </td>


                <td class="row_button"></td>



                <!--end of else for not cancelled-->
            </tr>



        </table>


        <div class="master_date mbHeaderBar">
            <div class="title_date ">Sun May 22, 2016</div>
            <div class="title_class">Class</div>
            <div class="title_staff">Instructor</div>
        </div>
        <table>
            <tr>

                <td class="row_date"><span class="mb_HolidayNotice">CIZE DANCE CERTIFICATION 9AM-5PM</span></td>

                <td class="row_class">

                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname"></span>
                    <span class="mb_le_staff_lastname"></span>
                </td>


                <td class="row_button"></td>



                <!--end of else for not cancelled-->
            </tr>



        </table>


        <div class="master_date mbHeaderBar">
            <div class="title_date ">Mon May 23, 2016</div>
            <div class="title_class">Class</div>
            <div class="title_staff">Instructor</div>
        </div>
        <table>
            <tr>

                <td class="row_date">8:30 am - 9:30 am</td>
                <td class="row_class">
                    Yoga Gentle Hatha Flow
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Jenn</span>
                    <span class="mb_le_staff_lastname">M</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">9:30 am - 10:00 am</td>
                <td class="row_class">
                    Zumba with Weights
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">10:00 am - 10:30 am</td>
                <td class="row_class">
                    Zumba
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:00 pm - 6:30 pm</td>
                <td class="row_class">
                    Core-Blast
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:30 pm - 7:30 pm</td>
                <td class="row_class">
                    Salsa Cubana
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Ariel</span>
                    <span class="mb_le_staff_lastname">Flores</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:30 pm - 7:30 pm</td>
                <td class="row_class">
                    Insanity Workout
                    <span class="sched_resource">STUDIO 2</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">7:30 pm - 8:30 pm</td>
                <td class="row_class">
                    Dance Fusion
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">JAY</span>
                    <span class="mb_le_staff_lastname">L.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>



        </table>


        <div class="master_date mbHeaderBar">
            <div class="title_date ">Tue May 24, 2016</div>
            <div class="title_class">Class</div>
            <div class="title_staff">Instructor</div>
        </div>
        <table>
            <tr>

                <td class="row_date">8:45 am - 9:30 am</td>
                <td class="row_class">
                    Mat Pilates
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Melissa</span>
                    <span class="mb_le_staff_lastname">S.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">9:30 am - 10:30 am</td>
                <td class="row_class">
                    Zumba
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Melissa</span>
                    <span class="mb_le_staff_lastname">S.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:30 pm - 7:30 pm</td>
                <td class="row_class">
                    Cardio Kick
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Heidi</span>
                    <span class="mb_le_staff_lastname">J.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">6:30 pm - 7:30 pm</td>
                <td class="row_class">
                    Body Toning
                    <span class="sched_resource">STUDIO 2</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Francisco</span>
                    <span class="mb_le_staff_lastname">F</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">7:30 pm - 8:30 pm</td>
                <td class="row_class">
                    Zumba
                    <span class="sched_resource">STUDIO 1</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Michel</span>
                    <span class="mb_le_staff_lastname">R</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>
            <tr>

                <td class="row_date">7:30 pm - 8:30 pm</td>
                <td class="row_class">
                    Cardio Boot Camp
                    <span class="sched_resource">STUDIO 2</span>
                </td>
                <td class="row_staff">
                    <span class="mb_le_staff_firstname">Leyder</span>
                    <span class="mb_le_staff_lastname">C.</span>
                </td>



                <td class="row_button">
                    <!-- which button is it? waitlist, class full, signup -->


                </td>

                <!-- if no signup for this one class, still put the td in there -->


                <!--end of else for not cancelled-->
            </tr>



        </table>

    </div>
</div>