<?php
/* @var $this yii\web\View */

$this->title = 'Class Descriptions';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="contact_us">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                    <div class="dv-h3"><?= $this->title;?></div>
                <div>
                    <script src="https://widgets.healcode.com/javascripts/healcode.js" type="text/javascript"></script>

                    <healcode-widget data-type="class_lists" data-widget-partner="mb" data-widget-id="b57019f24b" data-widget-version="0.1"></healcode-widget>

                </div>
            </div><!--col-sm-12-->

        </div><!--row-->
    </div><!--container-->
</section><!--S-rental-->
