<?php
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use \yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'Products';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<?php
$url = \yii\helpers\Url::to(['/product-buy']);
$format = <<< SCRIPT
function product_checkout(id)
{
    var ids = $("#product-list-"+id+" input[type='radio']:checked");
    if(ids.length <=0) {
        alert('Please choose a product!');
    } else {
        if(ids.val()) {
            window.location.href="$url"+"/"+ids.val();
        }
    }
}
SCRIPT;
$this->registerJs($format, \yii\web\View::POS_HEAD);
?>
<section class="S-rental">
    <div class="container">
        <div class="row">
            <?php if (Yii::$app->user->isGuest) { ?>
                <div class="col-sm-12">
                    <?= \frontend\components\LoginWidget::widget(); ?>
                </div>
            <?php } ?>

            <div class="col-sm-12 item-blk">
                <h3>Please choose an item to purchase:</h3>
                <?php foreach ($programs as $id => $program) {
                    $services = $api->getServices($id);
                    if(count($services) > 0) {
                    ?>
                    <div class="item-row">
                        <h4><?= $program; ?></h4>
                        <ul id="product-list-<?=$id?>">
                            <?php
                            foreach ($services as $service) {
                                if(isset($service['ProductID'])) {
                                ?>
                                <li>
                                    <input type="radio" name="product<?=$id?>" value="<?= $service['ProductID'] ?>">
                                    $<?= $service['Price'] ?> - <?= $service['Name'] ?>
                                </li>
                            <?php }
                            } if(isset($services['ProductID'])) { ?>
                                <li>
                                    <input type="radio" name="product" value="<?= $services['ProductID'] ?>">
                                    $<?= $services['Price'] ?> - <?= $services['Name'] ?>
                                </li>
                           <?php } ?>
                        </ul>
                        <a href="https://clients.mindbodyonline.com/classic/ws?studioid=37534&stype=41&sTG=<?= $id;?>" class="btn btn-default nxt-tbn">NEXT</a>
<!--                        javascript:product_checkout(--><?//=$id?><!--)-->
                    </div>
                    <!--REQ-form-->
                <?php } }?>
            </div><!--col-sm-10-->
        </div><!--row-->
    </div><!--container-->
</section>