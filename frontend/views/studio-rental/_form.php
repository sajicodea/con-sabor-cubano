<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin(['id' => 'sudo-rental-form', 'options' => ['class'=>'form-horizontal reg-fm']]); ?>
<div class="input-success">
    <?php if(Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success" role="alert">
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>
</div>
<?= $form->field($model, 'full_name',['template' => '<div class="form-group"><label class="col-sm-3 control-label">{label}</label><div class="col-sm-9">{input}{error}</div></div>'])->textInput(['maxlength' => true,'class'=>'form-control']); ?>

<?= $form->field($model, 'email',['template' => '<div class="form-group"><label class="col-sm-3 control-label">{label}</label><div class="col-sm-9">{input}{error}</div></div>'])->textInput(['maxlength' => true,'class'=>'form-control']); ?>

<?= $form->field($model, 'phone',['template' => '<div class="form-group"><label class="col-sm-3 control-label">{label}</label><div class="col-sm-9">{input}{error}</div></div>'])->textInput(['maxlength' => true,'class'=>'form-control']); ?>

<?= $form->field($model, 'event_description',['template' => '<div class="form-group"><label class="col-sm-3 control-label">{label}</label><div class="col-sm-9">{input}{error}</div></div>'])->textInput(['maxlength' => true,'class'=>'form-control']); ?>

<?= $form->field($model, 'no_attendees',['template' => '<div class="form-group"><label class="col-sm-3 control-label">{label}</label><div class="col-sm-9">{input}{error}</div></div>'])->textInput(['maxlength' => true,'class'=>'form-control']); ?>

<?= $form->field($model, 'requested_date',['template' => '<div class="form-group"><label class="col-sm-3 control-label">{label}</label><div class="col-sm-9">{input}{error}</div></div>'])->widget(\kartik\date\DatePicker::classname(), [
    //'options' => ['cla' => 'Enter birth date'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]
]);?>
<?php
$timeList = [
    'Morning (before noon',
    'Afternoon (between noon and 5 pm)',
    'Evening (between 5 pm and 10 pm)',
    'Late Evening (after 10 pm)',
    'All Day (morning to 5 pm)',
    'All Evening (5 pm until after 10 pm)'
];
$timeList = array_combine($timeList,$timeList);
?>
<?= $form->field($model, 'requested_time',['template' => '<div class="form-group"><label class="col-sm-3 control-label">{label}</label><div class="col-sm-9">{input}{error}</div></div>'])->dropDownList($timeList, ['class'=>'form-control']); ?>

<?= $form->field($model, 'about_us',['template' => '<div class="form-group"><label class="col-sm-3 control-label">{label}</label><div class="col-sm-9">{input}{error}</div></div>'])->textarea(['maxlength' => true,'class'=>'form-control']); ?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
    </div>
</div>
<?php ActiveForm::end(); ?>
