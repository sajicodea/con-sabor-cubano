<?php
/* @var $this yii\web\View */

$this->title = 'Studio Rental';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="S-rental">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Studio Rental</h3>


                <p>
                    Our new studio is located at 3550 San Pablo Dam Road, Suite F, El Sobrante, CA 94803, is
                    approximately 1400 square feet with mirrows open dance studio. The main studio has Sprung Wood
                    floors, specialty dance floors, and high performance sound systems..
                    <br><br>
                    CSCDF is an excellent rehearsal facility for various types of commercial production and is also used
                    as a location shoot for commercials, TV shows and movies. We have FREE parking and can be used for
                    your social events such as: private parties, auditions & more!
                </p>
                <div class="col-sm-3 stud-ren-blk">
                    <img src="<?= $this->theme->baseUrl; ?>/img/IMG_1535.jpg" width="100%">
                </div>
                <div class="col-sm-3 stud-ren-blk">
                    <img src="<?= $this->theme->baseUrl; ?>/img/IMG_1534.jpg" width="100%">
                </div>
            </div><!--col-sm-12-->
            <div class="col-sm-12">
                <h3 class="pay-head">pay for rental</h3>
                <a target="_blank" class="btn btn-default pay-btn" href=" https://clients.mindbodyonline.com/classic/ws?studioid=37534&stype=45">
                    Pay Here
                </a>
<!--                <button class="btn btn-default pay-btn" type="submit">Pay Here</button>-->
            </div>
            <div class="col-sm-10 request-form">
                <h4>Studio Rental Request Form</h4>

                <div class="REQ-form">
                    <?= $this->render('_form', [
                        'model' => $model
                    ])?>
                </div><!--REQ-form-->

            </div><!--col-sm-10-->
        </div><!--row-->
    </div><!--container-->
</section><!--S-rental-->