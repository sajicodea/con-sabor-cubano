<?php
/* @var $this yii\web\View */

$this->title = 'Gallery';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="S-rental">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Gallery</h3>

                <div class="gallery-list">
                    <?php foreach($galleries as $gallery) {?>
                        <div class="col-sm-3">
                            <img src="<?= $gallery->getImageThumbUrl(255,192)?>" class="img-responsive" alt=""/>
                        </div>
                    <?php }?>
                </div>
            </div><!--col-sm-12-->


        </div><!--row-->
    </div><!--container-->
</section>
