<?php
/* @var $this yii\web\View */

$this->title = 'Instructors';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>

<section class="inner-testimonials">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="dv-h3"><?= $this->title;?></div>
                <?php foreach($instructors as $key =>$instructor) {?>
                    <div class="row">
                        <?php if($key%2 == 1) {?>
                        <div class="col-sm-3">
                            <img width="100%" alt="" class="instructor-img" src="<?= $instructor->getImageThumbUrl(263,365)?>">
                        </div>
                        <div class="col-sm-9">
                            <h4 class="h-instr"><?= $instructor->name;?></h4>
                            <div><?= $instructor->description;?></div>
                        </div>
                        <?php } else {?>
                            <div class="col-sm-9">
                                <h4 class="h-instr"><?= $instructor->name;?></h4>
                                <div><?= $instructor->description;?></div>
                            </div>
                            <div class="col-sm-3">
                                <img width="100%" alt="" class="instructor-img" src="<?= $instructor->getImageThumbUrl(263,365)?>">
                            </div>
                        <?php }?>
                    </div>
                    <div class="separator"> </div>
                <?php }?>
            </div><!--col-sm-12-->


        </div><!--row-->
    </div><!--container-->
</section><!--S-rental-->
<div class="row">
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/ARIEL.jpg" class="instructor-img" width="100%" alt="">
    </div>
    <div class="col-sm-9">
        <h4>ARIEL FLORES</h4>
        <p>Ariel Flores comes from a family of dancers and musicians. Cuban music and dance are infused in his blood. He brings the pure heat and exotic flow of rhythm to the popular dances ; Cha Cha, Son, Rumba, Mambo, Salsa-Timba and Casino Rueda infused with dynamic passion as found in his native Havana, Cuba.<br>
            In 1994, Ariel embarked on a journey from Havana, Cuba, as a Balsero”, risking his life to leave Cuban shores for a better life in the U.S. He began his new life for a short time, in Miami and after giving Oregon, New Mexico, Arizona and Bakersfield a try,  found his home in California. He has taught numerous classes and workshops with a huge following in the bay area (Santa Cruz, Oakland, Berkeley and San Francisco) as well as performing with many companies, such as Susana Arenas Dance company and onstage with Tiempo Libre and Maykel Blanco from Cuba.<br>
            <br>
            He has a 22 year old daughter living in Miami and a nineteen year old step daughter in Solano County, where he lives with his fiancé. He graduated from CET and is certified in building maintenance. He enjoys boating and fishing, although dancing is his first preference.
        </p>
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-9">
        <h4>DALILA SOLIS</h4>
        <p>Dalila was born in Mexico, raised in Texas, and moved to California to study Anthropology. She fell head over heels in love with yoga in 2003 and has been practicing ever since. Dalila is trained as a yoga instructor and continues to study and explore many different styles of yoga--Hatha, Vinyasa, Bhakti, Kundalini, Yin and Restorative--weaving elements and synthesizing new teachings into her classes.<br>
            She also studies Thai massage as well as Ayurveda--living in tune with the seasons and elements and using food as medicine to heal and nourish. <br>
            Her studies in Ayurveda and the healing arts also inform her yoga classes. Dalila lives in Oakland with her boyfriend, two dogs and a cat.
            When Dalila is not on the mat, you can find her enjoying the outdoors, traveling, cooking, gardening, making art and loving life.
        </p>
    </div>
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/DALILA.jpg" class="instructor-img" width="100%" alt="">
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/DAWN.jpg" class="instructor-img" width="100%" alt="">
    </div>
    <div class="col-sm-9">
        <h4>DAWN WILLIAMS</h4>
        <p> If you ask her mother she will say that Dawn has been dancing since the womb. In high school, she started a hip hop dance team. Dawn has studied Afro-Cuban dance in Cuba. She learned to dance salsa in Puerto Rico. She has taught hip hop to youth in the U.S., Argentina and in Guinea, West Africa. Dawn taught English, Spanish, and French at the high school level for twelve years. She is also a co-founder of the Williams-Bah museum in Dalaba, Guinea. She recently finished her doctorate program at U.C. Berkeley in Education looking at trauma in schools. <br>
            The music in her classes are a reflection of her travels and her Black and Filipina roots. <br>
            Dawn is a mother of three beautiful children and the wife of Piper of the band Flipsyde. She is the business manager for their entertainment company Pipe Dreamz Entertainment and the director of Oakland Science &amp; Math Outreach.
        </p>
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-9">
        <h4>DENMIS BIAN SAVIGNE</h4>
        <p>Denmis Bain was born August 9th in Santiago de Cuba, Cuba. He grew up always surrounded by love which helped to stimulate his passion for the arts. Denmis was taught at a young age that happiness comes from within and the importance to live life to the fullest. He has embraced this concept and strived to pass on his love for life and express his happiness through dance. Denmis received the majority of his formal training in Cuba. He attended Regino Eladio Voti en Guantanamo, Escuela Nacional de Arte in Havana and was a part of Conjuncto Folklorico Nacional de Cuba. He has expertise in afro Cuban dance, folklore, ballet, jazz, modern dance, contemporary ballet, tap dance, hip hop, choreography and singing. Denmis is probably most known for his work with the Havana Nights Dance Company. He toured and performed in more than 16 different countries and was a lead dancer in about 450 performances of "Havana Nightclub - The Show". <br>
            The performances received standing ovations and outstanding reviews from critics and audiences all around. The tour ended in Las Vegas, Nevada with the intentions of returning back to Cuba afterwards. Political hostilities between the U.S. and Cuba landed all 53 performers in the middle of a dispute that forced them into making life changing decisions. In 2004 Denmis Bain was a part of the largest mass defection in the United States and the entire cast requested and were granted political asylum. Denmis has remained in the U.S. ever since, fulfilling his original goal of performing and touching the hearts of anyone he could.
        </p>
    </div>
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/DENMIS.jpg" class="instructor-img" width="100%" alt="">
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/JAY.jpg" class="instructor-img" width="100%" alt="">
    </div>
    <div class="col-sm-9">
        <h4>JAY LOPEZ</h4>
        <p> Meet Jay Francisco Lopez  he was born to a family of immigrants of Honduran descent in the Mission District of San Francisco but grew up in Richmond CA. A man of many talents, as he starred in touring theatre productions such as Grease,Raisin In The Sun, Disney's High School Musical 2: Live On Stage before transitioning to films and televisions, where he has been in ...a wide variety of roles, appearing in the hit TV series I (Almost) Got Away With it and many more,  Jay loves teaching Dance Fusion and I bet your wondering what is Dance Fusion??? Dance Fusion is a mixture of Hip-Hop, Latin, Reggae &amp; Old School  Every class feels like a party! Since discovering teaching  5 years ago, He has never felt more in shape or more confident about himself! Jay's Dance Fusion class the will keep you moving. <br>
            His classes are fast-paced and high-energy, but with easy to follow steps, so if you want to get in shape, twerk a little bit and get your soul energized while having fun, come join him and you ‘ll see what we mean we promise you will get hooked!!
        </p>
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-9">
        <h4>JENN MASON</h4>
        <p>Jenn was born in New York and raised in Southern California. After college, Jenn moved to Boston to continue her education. Upon graduating from graduate school she moved to San Pablo where she lives with her husband and two dogs. Jenn has a M.A in Women's Health and an M.S in Integrative Medicine and Health Sciences which guide her professional work as a lifestyle and wellness coach. Jenn is trained as a yoga instructor with a focus on Restorative yoga. In her spare time you can find her teaching Spin classes, hiking with her dogs, or cooking.
        </p>
    </div>
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/JENN.jpg" class="instructor-img" width="100%" alt="">
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/MICHEL.jpg" class="instructor-img" width="100%" alt="">
    </div>
    <div class="col-sm-9">
        <h4>MICHEL RODRIGUEZ</h4>
        <p> Michel was born in Havana, Cuba. His passion comes from his culture, music, and background in dancing salsa cubana. Michel's favorite sport is baseball.  He started playing baseball when he was eight years old. He went to school and continued to play baseball until he was twenty-five years old. He graduated from college and received his diploma. He's majored is in Physical Education and Sports for Athletes. As he  finished, school, he continued playing baseball professionally. Unfortunately, due to a shoulder injury, he had to stop playing ball.<br>
            <br>
            In 2007, he came to the States, with a desire to learn and continue school. He became a certified personal trainer in sports for athletes. Two years ago, he attended The National Personal Training Institute, a school in San Francisco. Later, he decided to do sport massage.  He registered at McKinnon Massage Institute. He became Zumba certified. Michel is fitness professional sharing his passion in fitness and dance. He has become a master in Zumba, creating his own combinations with Cuban flavor, as he was the owner of CON-SABOR-CUBANO, DANCE &amp; FITNESS STUDIO, located in El Sobrante, CA.
        </p>
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-9">
        <h4>ROYLAND LOBATO</h4>
        <p>Royland is originally from Guantanamo, Cuba, Royland is a founding member of the Havana-based dance company, 7 Potencias, an Afro-Cuban Folkloric dance and music ensemble. A graduate of the Escuela de Instructors de Arte de la Ciudad de La Habana, (School of Arts Instructors in Havana) with a degree in Education, specializing in dance. 7 Potencias, one of the most creative and acclaimed Cuban folkloric ensembles, both in Cuba and internationally.  A professional dancer with an expertise in Afro-Cuban folkloric and Cuban popular dance, Royland has also participated as a principal dancer as well as instructor, and invited guest in
            performances, workshops and classes throughout the greater Bay Area, Hawaii, New York, Cuba, and Mexico. Royland has been teaching throughout the Bay Area since 2005.
        </p>
    </div>
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/ROYLAND.jpg" class="instructor-img" width="100%" alt="">
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/WILLA.jpg" class="instructor-img" width="100%" alt="">
    </div>
    <div class="col-sm-9">
        <h4>WILLA JACOBS</h4>
        <p> Willa Willis-Jacobs aka Bontle is honored to be a part of the Con Sabor Cubano Dance &amp; Fitness team! She is a dancer and Licensed Zumba Instructor. She has a B.A. in Physical Education/ Dane and has been teaching throughtout the world for many years.  Sha has been a member of dance companies including Ballet Lisanga,  Ceedo Senegalese dance Co. and New Style Motherlode, just to name a few.  her  passion for dance and movememnt is undeniable. She brings creativity, uniqueness, high energy, positivity, great moves and love to all her classes.  If you want to sweat, have fun, burn calories, get "yo" groove on, this class is a must!
        </p>
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-9">
        <h4> HEIDI JANEIRO</h4>
        <p>Heidi was born in Santiago, Chile and grew up in Mendoza/ Buenos Aires/ Argentina. She lived in Manhattan, NYC for seven years and then came to the Bay Area, California. She has been here ever since. As a child, her family tradition, included dancing, and listening to music. It has always been part of her life, since she was brought up in a large family. At parties her family enjoyed dancing the night away. In school, she was an athlete, a leader in the dance community and a team player. She enjoyed basketball, softball and tennis. In the last 18 years. She earned a recognition in the fitness field as a Certified Fitness Instructor and Personal Trainer, teaching several fitness class formats. She's a committed fitness leader, who has a passion to teach people how to stay fit and live a healthy lifestyle. Graduated college with a physical education degree. She continues to stay educated by participating in many seminars and workshops about medical terminology and fitness. She worked in the medical field as CMA and Part-time she enjoys teaching Zumba Fitness, BOOT CAMP, INSANITY, P90X, PIYO and other fitness formats .<br>
            In addition,  she's a proud mother of two beautiful smart kids, who are now in college. She's currently teaches several classes a week, with personal training.  She loves to dance salsa . Both Michel and Heidi have been dancing together, since they met. They both enjoyed dancing and teaching salsa/fitness classes together.
        </p>
    </div>
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/heidi.jpg" class="instructor-img" width="100%" alt="">
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/melinda.jpg" class="instructor-img" width="100%" alt="">
    </div>
    <div class="col-sm-9">
        <h4>MELINDA GREFALDIA</h4>
        <p>elinda is a Bay Area native, born in San Francisco, CA. Her first stage performance was at the age of 5 years old and very first hip hop class at age 8. She carried the love of dance throughout her childhood and adult life. Melinda trained in various dance forms such as jazz, ballet, modern, tap, hip hop, ballroom, and Afro-Caribbean, to name a few. She worked well in her community in musical theatre and dance productions – performing and choreographing many numbers, and instructing dance. Melinda had the privilege to train with world renowned dance companies such as Alvin Ailey American Dance Theatre, Paul Taylor Dance Company, Ronald K. Brown / Evidence A Dance Company, and much more.
            Melinda is AFAA (Aerobics and Fitness Association of America) and CPR (cardiopulmonary resuscitation) certified through the American Heart Association. She earned an AA degree in Liberal Arts, BA in Theatre Arts (Dance), and Paralegal Certificate. Melinda earned several honors and awards during her college years. She now fulfills her dance career in dance fitness as a certified U-JAM Fitness® and LaBlast® Fitness instructor and still teaches dance and choreographs pieces for her community. Melinda is pleased to lead members and participants in health and fitness.
        </p>
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-9">
        <h4>  Anna Zehringer </h4>
        <p>200 RYT Anna Zehringer is a 200hr registered yoga teacher with Yoga Alliance. She did her 200hr training with Blooming Lotus Yoga School in Thailand in 2014 and she recently completed a50hr advanced yoga philosophy training with Laughing Lotus in San Francisco. She loves to combine her passion for travel and yoga, and has taught in California, Panama, and Guatemala.<br>
            Yoga has helped her more than anything to be mindful, peaceful, and connect with her authentic self and it is her mission to share this path with others. Anna has also studied holistic health withthe Institute of Integrated Nutrition and Thai yoga massage at the Yoga Forest in Guatemala.
        </p>
    </div>
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/anna.jpg" class="instructor-img" width="100%" alt="">
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/uma.jpg" class="instructor-img" width="100%" alt="">
    </div>
    <div class="col-sm-9">
        <h4>Uma Maharjan</h4>
        <p>I am Uma Maharjan from Nepal and I have been doing Zumba for nearly 4 years  when I was 200 pounds and after falling in love with it, I become license Zumba fitness instructor (Zin). I have passion for helping people of all sizes and backgrounds to feel good about themselves. As a managing director of moonlight pharmacy, Zumba seemed to fit right into my vision for helping people discover their worth from inside and out.<br>
            <br>
            I am blessed to have the opportunity to share the joy of dance through Latin and Bollywood music. I am not a dancer and you don’t have to be either. You just have to come ready to sweat and have a great time and consider yourself coordinated in order to enjoy the class and get an amazing workout. For me “dance is the medium through which my body communicates with my soul”.
            I look forward to dance, sweat and most of all having fun with all of you.
        </p>
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-9">
        <h4>Francisco Flores </h4>
        <p>My name is Francisco Flores, and I’ve been immersed in the world of fitness for more than 29 years. Over that time, I’ve helped thousands of people reach their fitness goals.  I’m ready to help you unlock your potential and reach your physical and mental best as well. From aerobics to step, from spinning to Pilates, and onto Zumba, Yoga, Body Pump, Cardio Kickboxing. I’ve been hands-on with all styles of fitness and exercise for a quarter of a century. With continued education, certification and training, I’ve learned that knowledge is power, and that’s what I help to show each and every one of my clients and pupils as well.
        </p>
    </div>
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/fransisco.jpg" class="instructor-img" width="100%" alt="">
    </div>
</div><div class="separator">
</div><div class="row">
    <div class="col-sm-3">
        <img src="/con-sabor-cubano/frontend/web/themes/user/img/lynder.jpg" class="instructor-img" width="100%" alt="">
    </div>
    <div class="col-sm-9">
        <h4> Leyder Chapman</h4>
        <p>Leyder Chapman has been working in the fitness industry for twenty years! He graduated from college in Havana, Cuba with a BA in physical education. At the age of 19 Leyder joined the Cuban National Basketball Team. He played for his country for four years before he defected to the United States in 2001. Since 2001 Leyder has used his physical fitness knowledge and degree in all sorts of ways and at various gyms in the Bay Area. Leyder currently works at Active Sport in Oakland, The Bay Club in SF, and Oakwood in Lafayette teaching salsa cardio classes, body conditioning classes, kickboxing classes, zuba classes, and boot camps. He is also a basketball coach/trainer for kids ages 4-18. Not only is Leyder Chapman very active in the field of fitness but he is also a singer. His stage name is Dos Four and his music is a combination of Cuban rhythms such as timba, reggaeton, merengue, and latin hip hop. Dos Four has performed all over the Bay Area as well as in Los Angeles, Chicago, Miami, Tampa, Orlando, and has had two tours across Europe. He is a talented athlete, trainer, and musician and is excited to bring his energy and talents to new gyms in the area!
        </p>
    </div>
</div><div class="separator">
</div>