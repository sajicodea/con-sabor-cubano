<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-group">
    <?php $form = ActiveForm::begin([
            'id' => 'register-client-form',
            'action' => Url::to(['site/register']),
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['site/ajax-register']),
        ]);
    ?>
    <div class="input-success"></div>
    <div id="signup-content">
        <?= $form->field($model, 'username')->textInput(['maxlength' => true,'class'=>'form-control']); ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true,'class'=>'form-control']); ?>
        <div class="input-group">
            <?= yii\helpers\Html::submitButton('Submit', ['class' => 'common-btn btn btn-info']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>