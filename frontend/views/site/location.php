<?php
/* @var $this yii\web\View */

$this->title = 'Location';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="contact_us">
    <div class="container">
        <div class="row">

            <div class="col-md-7 contact-left">
                <h3>Address</h3>
                <p>
                    3550 San Pablo Dam Road Suite F El Sobrante, CA 94803
                    510-222-6300
                </p>
            </div><!--col-md-5-->



            <div class=" col-sm-12 location_map">
                <div class="location-map-hd">Location Map</div><!--location-map-hd-->
                <div class="map">
                    <div id="map-canvas">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d786.401282711831!2d-122.319873!3d37.963005!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1461061211359" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div><!--map-canvas-->
                </div><!--map-->
            </div><!--location map-->

        </div><!--row-->
    </div><!--container-->
</section>