<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Register';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="contact_us">
    <div class="container">
        <div class="row">

            <div class="col-md-7 contact-left reg-fm">
                <h3>Don't have an account?</h3>
                <script src="https://widgets.healcode.com/javascripts/healcode.js" type="text/javascript"></script>

                <healcode-widget data-type="registrations" data-widget-partner="mb" data-widget-id="b56210f24b" data-widget-version="0.1"></healcode-widget>
            </div><!--col-md-5-->

            <div class="col-md-4 contact-right pull-right">
                <h3>Address</h3>

                <p>
                    3550 San Pablo Dam Road Suite F El Sobrante, CA 94803
                    <span>510-222-6300</span>
                </p>

                <div class="contact-img"><img src="<?= $this->theme->baseUrl; ?>/img/contact-img.jpg"
                                              class="img-responsive" alt=""/></div>
            </div><!--col-md-4 pull-right-->

        </div><!--row-->
    </div><!--container-->
</section>
