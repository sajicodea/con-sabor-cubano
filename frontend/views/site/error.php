<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<section class="S-rental">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="dv-h3"><?php //Html::encode($this->title) ?></div>
                <div class="dv-h3">
                    <?= nl2br(Html::encode($message)) ?>
                </div>
                <p>
                    The above error occurred while the Web server was processing your request.
                </p>
                <p>
                    Please contact us if you think this is a server error. Thank you.
                </p>
            </div><!--col-sm-12-->


        </div><!--row-->
    </div><!--container-->
</section><!--S-rental-->
