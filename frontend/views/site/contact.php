<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'javascript:void(0)'];
?>
<section class="contact_us">
    <div class="container">
        <div class="row">

            <div class="col-md-7 contact-left reg-fm">

                <h3>Contact Us</h3>
                <div class="input-success">
                    <?php if(Yii::$app->session->hasFlash('success')): ?>
                        <div class="alert alert-success" role="alert">
                            <?= Yii::$app->session->getFlash('success') ?>
                        </div>
                    <?php endif; ?>
                </div>
                <p>
                    Have a question, comment or suggestion? We'd love to hear from you! Please use the contact form and we will reply as soon as possible!
                </p>

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => 'Enter Your Name']) ?>

                <?= $form->field($model, 'email')->textInput([ 'placeholder' => 'Enter Email Address']) ?>

                <?= $form->field($model, 'phone')->textInput([ 'placeholder' => 'Enter Phone Number']) ?>

                <?= $form->field($model, 'body')->textArea(['rows' => 6, 'placeholder' => 'Message']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-default', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div><!--col-md-5-->

            <div class="col-md-4 contact-right pull-right">
                <h3>Address</h3>
                <p>
                    3550 San Pablo Dam Road Suite F El Sobrante, CA 94803
                    <span>510-222-6300</span>
                </p>
                <div class="contact-img"><img src="<?= $this->theme->baseUrl; ?>/img/contact-img.jpg" class="img-responsive" alt=""/></div>
            </div><!--col-md-4 pull-right-->

            <div class=" col-sm-12 location_map">
                <div class="location-map-hd">Location Map</div><!--location-map-hd-->
                <div class="map">
                    <div id="map-canvas">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d786.401282711831!2d-122.319873!3d37.963005!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1461061211359" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div><!--map-canvas-->
                </div><!--map-->
            </div><!--location map-->

        </div><!--row-->
    </div><!--container-->
</section>
