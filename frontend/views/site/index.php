<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Con Sabor Cubano - Dance and Fitness';
?>
<section class="welcome">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="welcome-img">
                    <div class="border-1"></div><!--border-1-->
                    <div class="register-btn"><a href="<?= Url::to(['/register'])?>">Register Now</a></div><!--register-btn-->
                    <img src="<?= $this->theme->baseUrl; ?>/img/welcome-image.jpg" class="img-responsive" alt="" width="100%"/>
                </div><!--welcome-img-->
            </div><!--col-sm-4-->
            <div class="col-sm-8 welcome-right">
                <h1>welcome to Con-Sabor-Cubano Dance </h1>

                <h3>Our Mission Statement</h3>

                <p>
                    Con-Sabor-Cubano, Dance & Fitness is a health & fitness venue that helps individuals attain one of
                    the greatest gifts of all; a good health, Personal gains, such as improved self-esteem, and
                    self-motivation. We offer a place where people can learn to dance, increase their fitness capacity.
                    Meet new people, have fun and feel comfortable. We offer a varied dance fitness program with price
                    options for all levels of interest, with greater emphasis on group classes and small package
                    sessions to reach dance skill objectives.
                </p>
                <a href="<?= Url::to(['/culture-and-history'])?>" class="read-more">Read More</a><!--read-more-->
            </div><!--col-sm-8 welcome-right-->
        </div><!--row-->
    </div><!--container-->
</section>

<section class="what_we_do">
    <div class="container">
        <div class="row">
            <h3>What We Do</h3>

            <p>Con-Sabor-Cubano, Dance & Fitness is a health & fitness venue that helps individuals attain one of the
                greatest gifts of all</p>

            <div class="what_do_slide">

                <div id="owl-what_do" class="owl-carousel owl-theme">
                    <div class="item slide-top">
                        <div class="border-2"></div><!--border-2-->
                        <img src="<?= $this->theme->baseUrl; ?>/img/what-1.jpg" class="img-responsive" alt=""/>

                        <div class="what-slide-content">
                            <button type="button">Group Classes</button>
                            <p>
                                Classes are offered at every level, from basic beginner through advanced. In addition,
                                specialty classes
                            </p>

                            <a href="<?= Url::to(['/class-description'])?>" class="read-more">Read More</a><!--read-more-->
                        </div><!--what-slide-content-->
                    </div><!--item-->
                    <div class="item slide-bottom">
                        <div class="border-2"></div><!--border-2-->
                        <img src="<?= $this->theme->baseUrl; ?>/img/what-2.jpg" class="img-responsive" alt=""/>

                        <div class="what-slide-content">
                            <button type="button">Private Lessons</button>
                            <p>
                                Classes are offered at every level, from basic beginner through advanced. In addition,
                                specialty classes
                            </p>

                            <a href="<?= Url::to(['/class-description'])?>" class="read-more">Read More</a><!--read-more-->
                        </div><!--what-slide-content-->
                    </div><!--item-->
                    <div class="item slide-top">
                        <div class="border-2"></div><!--border-2-->
                        <img src="<?= $this->theme->baseUrl; ?>/img/what-3.jpg" class="img-responsive" alt=""/>

                        <div class="what-slide-content">
                            <button type="button">PERSONAL TRAINING</button>
                            <p>
                                We can train you, in privacy and comfort of your own home.
                                We have affordable packages for everyone.
                            </p>

                            <a href="<?= Url::to(['/personal-training'])?>" class="read-more">Read More</a><!--read-more-->
                        </div><!--what-slide-content-->
                    </div><!--item-->
                    <div class="item slide-bottom">
                        <div class="border-2"></div><!--border-2-->
                        <img src="<?= $this->theme->baseUrl; ?>/img/what-1.jpg" class="img-responsive" alt=""/>

                        <div class="what-slide-content">
                            <button type="button">Group Classes</button>
                            <p>
                                Classes are offered at every level, from basic beginner through advanced. In addition,
                                specialty classes
                            </p>

                            <a href="<?= Url::to(['/class-description'])?>" class="read-more">Read More</a><!--read-more-->
                        </div><!--what-slide-content-->
                    </div><!--item-->
                </div>

            </div><!--what_do_slide-->

        </div><!--row-->
    </div><!--container-->
</section><!--what_we_do-->

<section class="customer-news-main">
    <div class="customer-news">

        <div class="row">
            <div class="col-md-4">
                <div class="customer slide-top">
                    <h3>Our happy customers</h3>

                    <div class="video">
                        <img src="<?= $this->theme->baseUrl; ?>/img/video1.jpg" class="img-responsive" alt=""/>
                    </div><!--video-->
                    <div class="video-icons">
                        <a href="#"><img src="<?= $this->theme->baseUrl; ?>/img/video-icon-1.jpg" alt=""/></a>
                        <a href="#"><img src="<?= $this->theme->baseUrl; ?>/img/video-icon-2.jpg" alt=""/></a>
                    </div><!--video-icons-->
                </div><!--customer-->
            </div><!--col-md-4-->

            <div class="news slide-bottom">
                <h3>News and Events</h3>
                <h6>Classes are offered at every level, from basic beginner</h6>

                <div class="news-in">
                    <img src="<?= $this->theme->baseUrl; ?>/img/news1.jpg" class="img-responsive" alt=""/>
                    <h5> IN 2016 WE ARE BRINGING SOME OF THE BEST GROUP CLASSES YOU <br>HAVE EVER TAKING..STARTING WITH P90X </h5>

                    <a href="<?= Url::to(['/news-events'])?>">Read More ></a>
                </div><!--news-in-->

                <div class="news-in">
                    <img src="<?= $this->theme->baseUrl; ?>/img/news2.jpg" class="img-responsive" alt=""/>
                    <h5>TRX SUSPENSION TRAINING COMING IN MID JANUARY 2016</h5>


                    <a href="<?= Url::to(['/news-events'])?>">Read More ></a>
                </div><!--news-in-->

                <div class="news-in">
                    <img src="<?= $this->theme->baseUrl; ?>/img/what-2.jpg" width="150" height="99" class="img-responsive" alt=""/>
                    <h5>PIYO LIVE!! COMING IN MARCH/APRIL 2016</h5>

                    <a href="<?= Url::to(['/news-events'])?>">Read More ></a>
                </div><!--news-in-->

            </div><!--news-->


        </div><!--row-->

    </div><!--customer-news-->
</section><!--customer-news-main-->

<section class="personal-training">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 slide-top">
                <h2>Need a PERSONAL TRAINING</h2>
                <h5>Con-Sabor-Cubano, Dance & Fitness is a health & fitness venue that helps individuals attain one of
                    the greatest gifts of all</h5>
                <a href="<?= Url::to(['/register'])?>">
                    <button class="register-now">Register Now</button>
                </a>
            </div><!--col-sm-12-->
        </div><!--row-->
    </div><!--container-->
</section><!--personal-training-->

<section class="popular-class">
    <div class="container">
        <div class="row">
            <h3>POPULAR CLASSES</h3>

            <p>Con-Sabor-Cubano, Dance & Fitness is a health & fitness venue that helps individuals attain one of the
                greatest gifts of all</p>

            <div class=" col-md-3">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="crossfit slide-bottom">
                            <img width="100%" src="https://clients.mindbodyonline.com/studios/CONSABORCUBANODANCEANDFITNESSSTUDIO/reservations/52.jpg?imageversion=1464040773" class="img-responsive" alt=""/>
                            <div class="crossfit-content">
                                <h6>Dance Fusion</h6>
                                <p>Dance Fusion draws on dance forms from all over the world (Caribbean, Brazilian, African, Middle Eastern, and more).
                                <a href="<?= Url::to(['/class/52'])?>">Read More</a>
                            </div><!--crossfit-content-->
                        </div><!--crossfit-->
                    </div><!--ol-sm-12-->
                </div><!--row-->
            </div><!--col-md-3-->

            <div class="col-md-4">
                <div class="row">
                    <div class="col-sm-12 slide-top">
                        <div class="salsa">
                            <img src="<?= $this->theme->baseUrl; ?>/img/salsa-1.jpg" class="img-responsive" alt=""/>
                            <h6><a href="<?= Url::to(['/salsa-lessons'])?>">SALSA Classes</a></h6>
                        </div><!--salsa-->
                        <div class="salsa1">
                            <img src="<?= $this->theme->baseUrl; ?>/img/salsa-2.jpg" class="img-responsive" alt=""/>
                            <h6><a href="<?= Url::to(['/salsa-lessons'])?>">SALSA LESSONS</a></h6>
                        </div><!--salsa-->
                    </div><!--ol-sm-12-->
                </div><!--row-->
            </div><!--col-md-4-->

            <div class="col-md-5">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="zomba-dance slide-bottom">
                            <div class="zomba-1">
                                <img src="<?= $this->theme->baseUrl; ?>/img/zomba-dance1.jpg" class="img-responsive" alt=""/>
                            </div><!--zomba-1-->
                            <div class="zomba-content">
                                <h6><a href="<?= Url::to(['/class/2'])?>">zumba Class</a></h6>

                                <p>
                                    When participants see a Zumba class in action, they can’t wait to give it a try. Zumba classes feature exotic rhythms set to high-energy Latin and international beats
                                    ...

                                </p>
                                <a href="<?= Url::to(['/class/2'])?>">Read More</a>
                            </div><!--zomba-content-->
                        </div><!--zomba-dance-->
                    </div><!--ol-sm-12-->
                </div><!--row-->
            </div><!--col-md-5-->

        </div><!--row-->
    </div><!--container-->
</section><!--popular-class-->

<section class="instructor-testimonials">
    <div class="container">
        <div class="row">

            <div class="col-md-5">
                <h3>Dance Instructor</h3><br>
                <span><img src="<?= $this->theme->baseUrl; ?>/img/instractor-img.jpg" class="img-responsive" alt=""/></span>

                <p>
                    Meet Willa Willis Jacobs, Certified Zumba Dance Instructor<br><br>

                    We are excited to have her become part our talented Dance & Fitness Instructors. She is a dancer and
                    Licensed Zumba instructor.
                </p>
                <a href="<?= Url::to(['/instructors'])?>">Read More ></a>
                <br>
                <br>
                <br>
            </div><!--col-md-5-->
            <div class="col-md-6 pull-right">
                <h3>Testimonials</h3><br>

                <div class="testimonials">
                    <div id="owl-testimonials" class="owl-carousel owl-theme">
                        <?php foreach($testimonials as $testimonial) {?>
                            <div class="item">
                                <span class="icons">"</span>
                                <?php $description = $testimonial->description;
                                    if(strlen($description) >200) {
                                        $description = substr($description,0,200).'...';
                                    }
                                ?>
                                <p><?= $description;?></p>

                            </div><!--item-->
                        <?php }?>

                    </div>
                </div><!--testimonials-->
            </div><!--col-md-7-->

        </div><!--row-->
    </div><!--container-->
</section><!--instructor-testimonials-->
<section class="testimonials-home-form "><!--testimonial form-->
    <div class="container home-links customer slide-top">
        <div class="col-sm-4">
            <a href="<?= Url::to(['/testimonials'])?>">
                <img src="<?= $this->theme->baseUrl; ?>/img/testi-ico.png" class="home-icons" width="100%">

                <h3>TESTIMONIALS</h3>
            </a>
        </div>

        <div class="col-sm-4">
            <a href="<?= Url::to(['/register'])?>">
                <img src="<?= $this->theme->baseUrl; ?>/img/reg-ico.png" width="100%" class="home-icons">

                <h3>BUY NOW OR REGISTER NOW</h3>
            </a>
        </div>

        <div class="col-sm-4">
            <a href="<?= Url::to(['/special-promotion'])?>">
                <img src="<?= $this->theme->baseUrl; ?>/img/special-ico.png" width="100%" class="home-icons">

                <h3>SPECIAL PROMOTIONS</h3>
            </a>
        </div>
    </div>
</section>
<?php if (isset($_GET['type']) && $_GET['type'] == 'login' && Yii::$app->user->isGuest) {
    $this->registerJs('
    $(document).ready(function(){
        $("#client-login").click();
    });
', \yii\web\View::POS_READY);
} ?>
